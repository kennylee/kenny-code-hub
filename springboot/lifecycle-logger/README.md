# lifecycle-logger

---

### **应用生命周期日志组件说明文档**

---

#### **1. 核心功能**
`AppLifecycleLogger` 是一个 Spring Boot 组件，用于记录应用全生命周期的关键事件日志，包括：
• **应用启动完成**（成功初始化后）
• **优雅停机**（主动关闭或收到终止信号）
• **异常终止**（如 OOM 导致 JVM 崩溃）

---

#### **2. 使用方式**
##### **(1) 引入依赖**
确保项目中包含 Spring Boot Actuator（用于优雅停机）：
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

##### **(2) 自动配置**
组件通过 `LifecycleLoggingAutoConfiguration` 自动生效，无需手动启用。  
确保组件所在包路径被 Spring Boot 扫描（默认扫描主类同级目录）。

---

#### **3. 日志输出说明**
| **场景**                | **触发条件**                 | **日志示例**                                 |
|-------------------------|------------------------------|---------------------------------------------|
| **应用启动成功**        | 服务初始化完成               | `应用启动完成! 当前环境: dev`                |
| **优雅停机**            | 调用 `/actuator/shutdown` 或 `kill -15` | `应用正在优雅停机...`                     |
| **OOM 异常终止**        | JVM 内存溢出                 | `检测到致命错误: OOM，应用即将终止`          |
| **强制终止**            | `kill -9` 或系统崩溃         | 无日志（JVM 强制终止无法捕获）              |

---

#### **4. 技术实现细节**
##### **(1) 监听 Spring 生命周期事件**
• `ApplicationReadyEvent`：应用启动完成事件。
• `ContextClosedEvent`：Spring 上下文关闭事件（优雅停机）。

##### **(2) 全局异常处理**
• 通过 `UncaughtExceptionHandler` 捕获 `OutOfMemoryError` 等致命错误。
• 使用 `AtomicBoolean` 保证 OOM 状态标记的线程安全。

##### **(3) JVM 关闭钩子**
• 注册 `Shutdown Hook` 在 JVM 终止前补充日志。

---

#### **5. 配置参数（可选）**
通过 `application.yml` 调整 Actuator 停机端点权限：
```yaml
management:
  endpoint:
    shutdown:
      enabled: true
  endpoints:
    web:
      exposure:
        include: shutdown
```

---

#### **6. 注意事项**
1. **`kill -9` 无法捕获**  
   强制终止信号（`SIGKILL`）会直接终止进程，组件无法记录日志。

2. **OOM 日志可靠性**  
   JVM 在 OOM 后可能无法正常执行日志写入操作，建议结合监控系统（如 Prometheus + AlertManager）告警。

3. **生产环境建议**  
   • 开启 Spring Boot Actuator 健康检查。
   • 配置 JVM 参数生成 Heap Dump（`-XX:+HeapDumpOnOutOfMemoryError`）。