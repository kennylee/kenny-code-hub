package cn.kennylee.codehub.springboot.lifecycle.logger;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * <p> 启动日志测试 </p>
 * <p>Created on 2025/3/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@ActiveProfiles("test")
class AppStartupLoggerTest {

    @Test
    void contextLoads() {
    }
}
