package cn.kennylee.codehub.springboot.lifecycle.logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p> SpringBoot单元测试入口 </p>
 * <p>Created on 2024/1/27.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootApplication
public class TestAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestAppApplication.class, args);
    }
}
