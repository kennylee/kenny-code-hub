package cn.kennylee.codehub.springboot.lifecycle.logger.config;

import cn.kennylee.codehub.springboot.lifecycle.logger.AppLifecycleLogger;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

/**
 * <p> 生命周期日志配置 </p>
 * <p>Created on 2025/3/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@AutoConfiguration
@ConditionalOnClass(AppLifecycleLogger.class)
@ConditionalOnMissingBean(AppLifecycleLogger.class)
public class LifecycleLoggingAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    AppLifecycleLogger startupLogger(Environment environment) {
        return new AppLifecycleLogger(environment);
    }
}
