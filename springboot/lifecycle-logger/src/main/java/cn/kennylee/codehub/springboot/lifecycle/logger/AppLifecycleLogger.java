package cn.kennylee.codehub.springboot.lifecycle.logger;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.system.SystemUtil;
import jakarta.servlet.ServletContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;

import java.util.concurrent.atomic.AtomicBoolean;

import static cn.hutool.core.util.RuntimeUtil.getUsableMemory;
import static cn.hutool.system.SystemUtil.*;

/**
 * 应用生命周期事件日志记录器.
 *
 * <p>监听并记录应用启动、关闭及致命异常事件，包括：</p>
 * <ul>
 *   <li>启动成功时打印环境信息</li>
 *   <li>优雅停机时记录关闭事件</li>
 *   <li>OOM等致命错误发生时输出异常日志</li>
 * </ul>
 * <p>注：调用 kill -9 无日志（JVM 强制终止，无法捕获）</p>
 * <p>技术实现：</p>
 * <ul>
 *   <li>通过 {@link EventListener} 监听Spring生命周期事件</li>
 *   <li>使用 {@link Runtime#addShutdownHook} 处理JVM关闭事件</li>
 *   <li>通过全局异常处理器捕获 {@link OutOfMemoryError}</li>
 * </ul>
 *
 * @author kennylee
 * @see ApplicationReadyEvent  应用启动完成事件
 * @see ContextClosedEvent      优雅停机触发事件
 * <p>Created on 2025/3/18.</p>
 * @since 0.0.1
 */
@Slf4j
public class AppLifecycleLogger {

    private static final AtomicBoolean SHUTDOWN_HOOK_REGISTERED = new AtomicBoolean(false);
    private static boolean shutdownLogged = false;

    private static final AtomicBoolean OOM_TRIGGERED = new AtomicBoolean(false);

    private static final String PAD_STR = "=";
    private static final int LINE_LENGTH = 75;

    private final Environment environment;

    /**
     * 是否已触发 OOM
     */
    public AppLifecycleLogger(Environment environment) {
        this.environment = environment;
        registerGlobalExceptionHandler();
        registerShutdownHook();

        // 确保全局只注册一次关闭钩子
        if (SHUTDOWN_HOOK_REGISTERED.compareAndSet(false, true)) {
            Runtime.getRuntime().addShutdownHook(new Thread(this::logShutdownIfNeeded));
        }
    }

    /**
     * 监听应用启动完成事件，输出环境信息.
     */
    @EventListener(ApplicationReadyEvent.class)
    public void logStartupInfo() {
        if (log.isInfoEnabled()) {
            log.info(new ApplicationInfo().toString());
        }
    }

    /**
     * 监听优雅停机事件
     */
    @EventListener(ContextClosedEvent.class)
    public void logGracefulShutdown(ContextClosedEvent event) {
        // 仅处理根上下文关闭事件
        if (event.getApplicationContext().getParent() == null) {
            logShutdownIfNeeded();
        }
    }

    private synchronized void logShutdownIfNeeded() {
        if (!shutdownLogged) {
            log.info("|----------------------------------------|");
            log.info("| 【{}服务】正在优雅停机...                     ", getAppName());
            log.info("|----------------------------------------|");
            shutdownLogged = true;
        }
    }

    /**
     * 注册全局异常处理器（捕获 OOM）
     */
    private void registerGlobalExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            if (throwable instanceof OutOfMemoryError) {
                OOM_TRIGGERED.set(true);
                log.error("|----------------------------------------|");
                log.error("| 【{}服务】检测到致命错误: OOM，应用即将终止         ", getAppName());
                log.error("| 错误信息: {}                            ", throwable.getMessage());
                log.error("|----------------------------------------|");
                log.error(throwable.getMessage(), throwable);
            }
        });
    }

    /**
     * 注册 JVM 关闭钩子（处理 OOM 终止）
     */
    private void registerShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (OOM_TRIGGERED.get()) {
                log.error("|----------------------------------------|");
                log.error("| 【{}服务】因 OOM 异常终止                      ", getAppName());
                log.error("|----------------------------------------|");
            }
        }));
    }

    private class ApplicationInfo {

        private void addJavaRuntimeMemoryInfo() {
            log.info(StrUtil.center(" Java Runtime Memory ", LINE_LENGTH, PAD_STR));
            log.info("Max Memory:    {}", FileUtil.readableFileSize(getMaxMemory()));
            log.info("Total Memory:    {}", FileUtil.readableFileSize(getTotalMemory()));
            log.info("Free Memory:    {}", FileUtil.readableFileSize(getFreeMemory()));
            log.info("Usable Memory:    {}", FileUtil.readableFileSize(getUsableMemory()));
        }

        private void addWebServerInfo() {
            ServletContext servletContext = SpringUtil.getBean(ServletContext.class);
            log.info(StrUtil.center(" WebServer Info ", LINE_LENGTH, PAD_STR));
            log.info("Web Server: {}", servletContext.getServerInfo());
            log.info("Servlet Specification Version: {}.{}", servletContext.getMajorVersion(), servletContext.getMinorVersion());

            log.info("Java Version: {}", SystemUtil.getJavaInfo().getVersion());
            log.info("Java Runtime: {} (build {})", SystemUtil.getJavaRuntimeInfo().getName(), SystemUtil.getJavaRuntimeInfo().getVersion());
            log.info("Java VM: {} (build {}, {})", SystemUtil.getJvmInfo().getName(), SystemUtil.getJvmInfo().getVersion(), SystemUtil.getJvmInfo().getInfo());
        }

        @Override
        public String toString() {
            this.addWebServerInfo();
            this.addJavaRuntimeMemoryInfo();

            return StrUtil.center(
                StrUtil.format(" [【{}服务】启动]Started Successfully, 当前环境: {}", getAppName(), getActiveProfiles()),
                LINE_LENGTH, PAD_STR);
        }
    }

    private String getActiveProfiles() {
        return String.join(",", environment.getActiveProfiles());
    }

    private String getAppName() {
        return environment.getProperty("spring.application.name", "");
    }
}
