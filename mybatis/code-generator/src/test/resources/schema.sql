create table t_member
(
    id                     bigint           not null
        primary key,
    user_id                bigint           not null comment '用户id',
    user_name              varchar(10)      null comment '用户姓名',
    user_card_num          varchar(32)      null comment '会员卡号',
    user_head_portrait     varchar(255)     not null comment '用户头像',
    user_nickname          varchar(32)      not null comment '用户昵称',
    user_phone             varchar(16)      null comment '用户手机号',
    user_authority         varchar(64)      null comment '用户权限',
    gender                 int              null comment '性别',
    balance                bigint           null comment '用户余额',
    growth_value           bigint           null comment '成长值',
    remark                 varchar(255)     null comment '备注',
    consume_count          int              null comment '消费次数',
    deal_total_money       bigint           null comment '交易总金额',
    confine_type           int              null comment '限制类型',
    growth_pay_order_count int    default 0 null comment '奖励成长值的支付订单数',
    growth_pay_order_money bigint default 0 null comment '奖励成长值的实付金额',
    integral_total         bigint           null comment '用户总积分值',
    distribution_count     int              null comment '分销次数',
    last_deal_time         timestamp        null comment '最后交易时间',
    birthday               date             null comment '生日',
    `explain`              varchar(255)     null comment '用户拉黑说明',
    create_time            timestamp        not null comment '创建时间',
    update_time            timestamp        not null comment '更新时间',
    version                int              not null comment '乐观锁版本号',
    deleted                tinyint(1)       not null comment '逻辑删除',
    constraint uk_user_id
        unique (user_id)
)
    comment '会员表' row_format = DYNAMIC;

create table t_shop
(
    id              bigint        not null
        primary key,
    no              char(16)      not null comment '店铺编号',
    company_name    varchar(50)   not null comment '企业名称',
    name            varchar(50)   not null comment '店铺名称',
    user_id         bigint        not null comment '店铺管理员用户id',
    contract_number varchar(30)   not null comment '联系电话',
    shop_mode       tinyint       null comment '店铺运营模式',
    status          tinyint       not null comment '状态 审核中0  1.正常 -1.禁用 -2审核拒绝',
    location        point         not null comment '定位',
    area            json          not null comment '省市区 数组',
    address         varchar(200)  not null comment '详细地址',
    logo            varchar(2048) not null comment 'logo url',
    briefing        varchar(200)  not null comment '介绍',
    head_background varchar(2048) null comment '店铺头部背景',
    start           time          null comment '营业开始时间',
    shop_type       int           null comment '店铺类型',
    extraction_type int           null comment '抽取类型：0-类目抽佣 1-订单销售额抽佣',
    draw_percentage int           null comment '抽成百分比',
    end             time          null comment '营业结束时间',
    new_tips        varchar(255)  null comment '上新提示',
    mode            int           null comment '经营模式',
    create_time     timestamp     not null comment '创建时间/申请时间',
    update_time     timestamp     not null comment '更新时间',
    version         int           not null comment '乐观锁版本号',
    deleted         tinyint(1)    not null comment '逻辑删除',
    extra           json          null comment '额外信息',
    constraint no
        unique (no)
)
    comment '商家注册信息' row_format = DYNAMIC;