package cn.kennylee.codehub.mybatis.baseusage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <p> Mybatis-Plus 配置 </p>
 * <p>Created on 2024/3/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Configuration
@MapperScan("cn.kennylee.codehub.mybatis.baseusage.mapper")
@EnableTransactionManagement // 支持事务 @Transactional
public class MybatisPlusConfigs {
}
