package cn.kennylee.codehub.mybatis.baseusage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseUsageApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseUsageApplication.class, args);
	}

}
