package cn.kennylee.codehub.mybatis.baseusage.eo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * <p> User </p>
 * <p>Created on 2024/3/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Data
@TableName("users")
public class UserEo {
    private Integer id;
    /**
     * 用户名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 邮箱地址
     */
    @TableField(value = "email")
    private String email;
}
