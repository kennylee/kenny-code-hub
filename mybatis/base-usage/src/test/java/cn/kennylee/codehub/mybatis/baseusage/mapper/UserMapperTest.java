package cn.kennylee.codehub.mybatis.baseusage.mapper;

import cn.kennylee.codehub.mybatis.baseusage.eo.UserEo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * <p> UserMapper单元测试类 </p>
 * <p>Created on 2024/3/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@Transactional
class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    @Test
    void testSelectOne() {
        final QueryWrapper<UserEo> orderQuery1 = new QueryWrapper<UserEo>()
            .eq("user_name", "user1");
        final QueryWrapper<UserEo> orderQuery2 = new QueryWrapper<UserEo>()
            .eq("user_name", LocalDateTime.now().toString());

        Assertions.assertNotNull(userMapper.selectOne(orderQuery1));
        Assertions.assertNull(userMapper.selectOne(orderQuery2));
    }
}
