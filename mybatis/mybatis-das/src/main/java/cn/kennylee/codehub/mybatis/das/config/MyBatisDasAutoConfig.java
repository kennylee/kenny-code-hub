package cn.kennylee.codehub.mybatis.das.config;

import cn.kennylee.codehub.common.das.DasDbType;
import cn.kennylee.codehub.common.das.DasStrategyFactory;
import cn.kennylee.codehub.mybatis.das.extension.BatchSaveIdProvider;
import cn.kennylee.codehub.mybatis.das.extension.MyBatisDasQueryStrategy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;

/**
 * <p> MyBatis-DAS自动配置 </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Configuration
public class MyBatisDasAutoConfig implements SmartInitializingSingleton, ApplicationContextAware {

    private ApplicationContext applicationContext;

    /**
     * 实例化MyBatis查询策略
     *
     * @return MyBatisDasQueryStrategy
     */
    @Bean
    @ConditionalOnMissingBean
    @SuppressWarnings("rawtypes")
    public MyBatisDasQueryStrategy myBatisDasQueryStrategy() {
        return new MyBatisDasQueryStrategy();
    }

    /**
     * 配置批量保存的ID提供者
     *
     * @return BatchSaveIdProvider
     */
    @Bean
    @ConditionalOnMissingBean
    @SuppressWarnings("rawtypes")
    public BatchSaveIdProvider batchSaveIdProvider() {
        return new BatchSaveIdProvider();
    }

    /**
     * 注册MyBatis查询策略到DasStrategyFactory
     */
    @Override
    @SuppressWarnings({"unchecked"})
    public void afterSingletonsInstantiated() {
        // 注册MyBatis查询策略
        DasStrategyFactory.getInstance().registerStrategy(DasDbType.MYSQL, applicationContext.getBean(MyBatisDasQueryStrategy.class));
    }

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
