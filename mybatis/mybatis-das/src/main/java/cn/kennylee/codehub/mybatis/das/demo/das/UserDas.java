package cn.kennylee.codehub.mybatis.das.demo.das;

import cn.kennylee.codehub.mybatis.das.demo.eo.UserEo;
import cn.kennylee.codehub.mybatis.das.extension.AbstractDas;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p> 用户数据访问服务 </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Service
@Slf4j
public class UserDas extends AbstractDas<UserEo, Long> {

}
