package cn.kennylee.codehub.mybatis.das.demo.eo;

import cn.kennylee.codehub.mybatis.das.eo.BaseEo;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p> User </p>
 * <p>Created on 2024/3/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Data
@TableName("users")
@EqualsAndHashCode(callSuper = true)
public class UserEo extends BaseEo<Long> {

    /**
     * 用户名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 邮箱地址
     */
    private String email;

    private Integer age;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
