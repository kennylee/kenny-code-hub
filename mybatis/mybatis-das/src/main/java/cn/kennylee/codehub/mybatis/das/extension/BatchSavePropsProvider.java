package cn.kennylee.codehub.mybatis.das.extension;

import cn.kennylee.codehub.mybatis.das.eo.BaseEo;
import com.baomidou.mybatisplus.core.metadata.TableInfo;

import java.io.Serializable;
import java.util.Collection;

/**
 * <p> 批量保存的属性字段扩展提供者 </p>
 * <p>Created on 2025/2/10.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
public interface BatchSavePropsProvider<T extends BaseEo<P>, P extends Serializable> {

    /**
     * 设置批量保存的属性字段
     *
     * @param entities  实体集合
     * @param tableInfo 表信息
     */
    void provide(Collection<T> entities, TableInfo tableInfo);
}
