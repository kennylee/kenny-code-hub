package cn.kennylee.codehub.mybatis.das.eo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p> 基础EO类  </p>
 * <p>Created on 2024/4/21.</p>
 *
 * @param <P> 主键类型
 * @author kennylee
 * @since 0.0.1
 */
@Setter
@Getter
public abstract class BaseEo<P extends Serializable> {

    @TableId
    private P id;
}
