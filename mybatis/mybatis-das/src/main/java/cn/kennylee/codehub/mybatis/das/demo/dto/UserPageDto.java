package cn.kennylee.codehub.mybatis.das.demo.dto;

import cn.kennylee.codehub.common.das.dto.PageDto;
import lombok.Getter;
import lombok.Setter;

/**
 * <p> UserPageDto </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Getter
@Setter
public class UserPageDto extends PageDto {
    /**
     * 用户名精确查询
     */
    private String userName;

    /**
     * 邮箱精确查询
     */
    private String email;

    /**
     * 用户名模糊查询
     */
    private String userNameLike;

    /**
     * 用户名后缀模糊查询
     */
    private String userNameSufLike;
}
