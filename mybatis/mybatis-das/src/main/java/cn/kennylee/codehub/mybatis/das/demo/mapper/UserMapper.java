package cn.kennylee.codehub.mybatis.das.demo.mapper;

import cn.kennylee.codehub.mybatis.das.demo.eo.UserEo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p> 用户mapper </p>
 * <p>Created on 2024/2/28.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEo> {

}
