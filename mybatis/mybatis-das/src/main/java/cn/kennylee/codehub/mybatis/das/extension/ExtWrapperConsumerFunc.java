package cn.kennylee.codehub.mybatis.das.extension;

import cn.kennylee.codehub.mybatis.das.eo.BaseEo;
import com.github.yulichang.query.MPJQueryWrapper;

/**
 * <p> 增加wrapper的方法 </p>
 * <p>Created on 2024/12/7.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@FunctionalInterface
@SuppressWarnings("rawtypes")
public interface ExtWrapperConsumerFunc<T extends BaseEo> {
    /**
     * 接收wrapper
     * @param wrapper wrapper
     */
    void accept(MPJQueryWrapper<T> wrapper);
}
