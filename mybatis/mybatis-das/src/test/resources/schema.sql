DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users
(
    id       BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(50)  NOT NULL,
    email    VARCHAR(100) NOT NULL,
    age     INT,
    create_time TIMESTAMP
);
