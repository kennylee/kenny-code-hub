package cn.kennylee.codehub.mybatis.das.extension;

import cn.hutool.json.JSONUtil;
import cn.kennylee.codehub.mybatis.das.demo.das.UserDas;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * <p> ComBaseDas单元测试 </p>
 * <p>Created on 2025/2/19.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@Transactional
@Slf4j
class ComBaseDasTest {

    @Resource
    private UserDas userDas;

    @Test
    @DisplayName("测试查询第一个")
    void testFindOne() {
        // given
        // when
        var userEo = userDas.findOne();

        // then
        assertNotNull(userEo);
        log.info("查询结果: {}", JSONUtil.toJsonStr(userEo));
    }
}
