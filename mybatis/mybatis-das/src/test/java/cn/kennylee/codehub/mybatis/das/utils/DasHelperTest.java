package cn.kennylee.codehub.mybatis.das.utils;

import cn.kennylee.codehub.mybatis.das.demo.eo.UserEo;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * <p> DasHelper测试类 </p>
 * <p>Created on 2025/2/19.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@Transactional
@Slf4j
class DasHelperTest {

    @Resource
    private DataSource dataSource;

    @Test
    @DisplayName("测试获取数据库类型")
    void testGetDbType() throws SQLException {
        // given

        // when
        var dbType = DasHelper.getDbType(dataSource);

        // then
        Assertions.assertNotNull(dbType);
        log.info("数据库类型: {}", dbType);
    }

    @Test
    @DisplayName("测试是否支持MySQL语法")
    void testIsSupportsMySqlSyntax() {
        // given

        // when
        var isSupportsMySqlSyntax = DasHelper.isSupportsMySqlSyntax(dataSource);

        // then
        Assertions.assertTrue(isSupportsMySqlSyntax);
    }

    @Test
    @DisplayName("测试获取实体类主键数据库名称")
    void testGetEntityPkDbName() {
        // given
        var entityClass = UserEo.class;

        // when
        var pkDbName = DasHelper.getEntityPkDbName(entityClass);

        // then
        Assertions.assertNotNull(pkDbName);
        log.info("实体类主键数据库名称: {}", pkDbName);
    }

    @Test
    @DisplayName("测试获取实体类主键值")
    void testGetEntityPkValue() {
        // given
        var entity = new UserEo();
        entity.setId(1L);

        // when
        var pkValue = DasHelper.getEntityPkValue(entity);

        // then
        Assertions.assertEquals(entity.getId(), pkValue);
    }
}
