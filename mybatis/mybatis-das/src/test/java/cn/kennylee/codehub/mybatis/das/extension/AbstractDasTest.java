package cn.kennylee.codehub.mybatis.das.extension;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONUtil;
import cn.kennylee.codehub.mybatis.das.demo.das.UserDas;
import cn.kennylee.codehub.mybatis.das.demo.dto.UserPageDto;
import cn.kennylee.codehub.mybatis.das.demo.eo.UserEo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p> AbstractDas单元测试 </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@Transactional
@Slf4j
class AbstractDasTest {

    @Resource
    private UserDas userDas;

    @Test
    @DisplayName("测试分页查询 -- 冒烟")
    void testFindPage() {
        // given
        UserPageDto userPageDto = new UserPageDto();

        // when
        IPage<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        log.info("分页查询结果: \n{}", JSONUtil.toJsonPrettyStr(userEoPage.getRecords()));
        Assertions.assertEquals(2, userEoPage.getRecords().size());
        Assertions.assertEquals(2, userEoPage.getTotal());
    }

    @Test
    @DisplayName("测试分页查询 -- 不进行count")
    void testFindPage_2() {
        // given
        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setSearchCount(false);

        // when
        IPage<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        Assertions.assertEquals(2, userEoPage.getRecords().size());
        Assertions.assertEquals(0, userEoPage.getTotal());
    }

    @Test
    @DisplayName("测试分页查询 -- 全模糊查询")
    void testFindPage_3() {
        // given
        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setUserNameLike("user");

        // when
        IPage<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        Assertions.assertEquals(2, userEoPage.getRecords().size());
        Assertions.assertEquals(2, userEoPage.getTotal());
    }

    @Test
    @DisplayName("测试分页查询 -- 后模糊查询")
    void testFindPage_4() {
        // given
        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setUserNameSufLike("user");

        // when
        IPage<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        Assertions.assertEquals(2, userEoPage.getRecords().size());
        Assertions.assertEquals(2, userEoPage.getTotal());
    }

    @Test
    @DisplayName("测试批量插入数据 -- 冒烟，包含id")
    void testSaveBatch() {
        // given
        int count = 201;
        List<UserEo> userEos = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            userEos.add(generateTestUserEo(true));
        }

        // when
        var result = userDas.saveBatch(userEos);

        // then
        Assertions.assertTrue(result);
        Assertions.assertEquals(count, userDas.countByIds(userEos.stream().map(UserEo::getId).collect(Collectors.toSet())));
        // 统计age为null的数据
        long nullAgeCount = userDas.lambda().isNull(UserEo::getAge).count();
        Assertions.assertTrue(nullAgeCount > 0);
        Assertions.assertNotEquals(count, nullAgeCount);
        // 统计createTime为null的数据
        long nullCreateTimeCount = userDas.lambda().isNull(UserEo::getCreateTime).count();
        Assertions.assertTrue(nullCreateTimeCount > 0);
        Assertions.assertNotEquals(count, nullCreateTimeCount);
        UserEo one = userDas.lambda().isNotNull(UserEo::getCreateTime).last(" LIMIT 1").one();
        log.info("抽样数据结构: \n{}", JSONUtil.toJsonStr(one, JSONConfig.create().setDateFormat("yyyy-MM-dd HH:mm:ss")));
    }

    @Test
    @DisplayName("测试批量插入数据 -- 分片测试")
    void testSaveBatch_split() {
        // given
        int count = 201;
        List<UserEo> userEos = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            userEos.add(generateTestUserEo(true));
        }

        // when
        var result = userDas.saveBatch(userEos, 10);

        // then
        Assertions.assertTrue(result);
        Assertions.assertEquals(count, userDas.countByIds(userEos.stream().map(UserEo::getId).collect(Collectors.toSet())));
        // 统计age为null的数据
        long nullAgeCount = userDas.lambda().isNull(UserEo::getAge).count();
        Assertions.assertTrue(nullAgeCount > 0);
        Assertions.assertNotEquals(count, nullAgeCount);
    }

    @Test
    @DisplayName("测试批量插入数据 -- 没有id")
    void testSaveBatch_notId() {
        // given
        int count = 201;
        List<UserEo> userEos = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            userEos.add(generateTestUserEo(false));
        }

        // when
        var result = userDas.saveBatch(userEos);

        // then
        Assertions.assertTrue(result);
        Assertions.assertEquals(count, userDas.countByIds(userEos.stream().map(UserEo::getId).collect(Collectors.toSet())));
        // 统计age为null的数据
        long nullAgeCount = userDas.lambda().isNull(UserEo::getAge).count();
        Assertions.assertTrue(nullAgeCount > 0);
        Assertions.assertNotEquals(count, nullAgeCount);
    }

    private UserEo generateTestUserEo(boolean hasId) {
        UserEo userEo = new UserEo();
        if (hasId) {
            userEo.setId(IdUtil.getSnowflakeNextId());
        }
        userEo.setUserName("test_user#" + RandomUtil.randomNumbers(6));
        userEo.setEmail(userEo.getUserName() + "@test.com");
        userEo.setAge(RandomUtil.randomBoolean() ? RandomUtil.randomInt(18, 30) : null);
        userEo.setCreateTime(RandomUtil.randomBoolean() ? LocalDateTime.now() : null);
        return userEo;
    }
}
