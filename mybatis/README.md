# MyBatis使用案例

## 模块说明

* [base-usage](base-usage): 最小化配置例子，单元测试用h2
* [mybatis-das](mybatis-das): 提供基于MyBatis的数据访问服务，增强基础的数据访问能力；增加一些MyBatis配置，如分页插件。
