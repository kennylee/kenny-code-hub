package cn.kennylee.codehub.cache.cacheservice;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.embedded.Redis;
import redis.embedded.RedisServer;

@SpringBootTest
class CacheServiceApplicationTests {

    private static Redis redisServer;

    @BeforeAll
    static void setUp() throws Exception {
        // 启动嵌入式 Redis
        redisServer = RedisServer.newRedisServer().setting("maxmemory 128M").build();
        redisServer.start();
    }

    @AfterAll
    static void tearDown() throws Exception {
        // 停止嵌入式 Redis
        redisServer.stop();
    }

    @Test
    void contextLoads() {
    }

}
