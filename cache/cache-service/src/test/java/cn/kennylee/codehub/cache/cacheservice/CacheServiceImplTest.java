package cn.kennylee.codehub.cache.cacheservice;

import cn.hutool.core.util.RandomUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import redis.embedded.Redis;
import redis.embedded.RedisServer;

import java.time.Duration;
import java.util.List;

/**
 * <p> CacheServiceImpl单元测试类 </p>
 * <p>Created on 2025/2/11.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@Slf4j
class CacheServiceImplTest {

    private static Redis redisServer;

    private CacheService cacheService;

    @BeforeEach
    void setUp() {
        cacheService = CacheHelper.getCache();
        log.info("cacheService: {}", cacheService);
    }

    @BeforeAll
    static void setUpAll() throws Exception {
        // 启动嵌入式 Redis
        redisServer = RedisServer.newRedisServer().setting("maxmemory 128M").build();
        redisServer.start();
    }

    @AfterAll
    static void tearDown() throws Exception {
        // 停止嵌入式 Redis
        redisServer.stop();
    }

    @Test
    @DisplayName("测试设置缓存 -- 冒烟测试")
    void testSetCache() {
        // given
        String testKey = "testKey#" + RandomUtil.randomString(5);
        String testValue = "testValue#" + RandomUtil.randomString(5);

        // when
        Assertions.assertFalse(cacheService.exists(testKey));
        cacheService.setCache(testKey, testValue, Duration.ofMinutes(1));

        // then
        Assertions.assertEquals(testValue, cacheService.getCache(testKey));
    }

    @Test
    @DisplayName("测试设置对象 -- 冒烟测试")
    void testSetObject() {
        // given
        var user = new User();
        user.setName("testName#" + RandomUtil.randomString(5));
        user.setAge(RandomUtil.randomInt(1, 100));

        String testKey = "testKey#" + RandomUtil.randomString(5);

        // when
        Assertions.assertFalse(cacheService.exists(testKey));
        cacheService.setObject(testKey, user, Duration.ofMinutes(1));

        // then
        User cacheUser = cacheService.getObject(testKey, User.class);
        Assertions.assertNotNull(cacheUser);
        Assertions.assertEquals(user.getName(), cacheUser.getName());
        Assertions.assertEquals(user.getAge(), cacheUser.getAge());
    }

    @Test
    @DisplayName("测试获取对象列表 -- 冒烟测试")
    void testGetList() {
        // given
        var user = new User();
        user.setName("testName#" + RandomUtil.randomString(5));
        user.setAge(RandomUtil.randomInt(1, 100));
        List<User> users = List.of(user);

        String testKey = "testKey#" + RandomUtil.randomString(5);

        // when
        Assertions.assertFalse(cacheService.exists(testKey));
        cacheService.setObject(testKey, users, Duration.ofMinutes(1));

        // then
        List<User> cacheUsers = cacheService.getList(testKey, User.class);
        Assertions.assertNotNull(cacheUsers);
        Assertions.assertEquals(1, cacheUsers.size());
        Assertions.assertEquals(user.getName(), cacheUsers.get(0).getName());
        Assertions.assertEquals(user.getAge(), cacheUsers.get(0).getAge());
    }

    @Getter
    @Setter
    public static class User {
        private String name;
        private int age;
    }
}
