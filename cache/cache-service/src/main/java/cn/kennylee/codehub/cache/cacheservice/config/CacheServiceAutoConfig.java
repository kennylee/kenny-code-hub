package cn.kennylee.codehub.cache.cacheservice.config;

import cn.kennylee.codehub.cache.cacheservice.CacheHelper;
import cn.kennylee.codehub.cache.cacheservice.CacheService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Objects;

/**
 * <p> CacheService配置类 </p>
 * <p>Created on 2025/2/11.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Configuration
@Import({CacheHelper.class})
public class CacheServiceAutoConfig {

    @Bean
    @ConditionalOnMissingBean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        return redisTemplate;
    }

    /**
     * 定义默认应用缓存服务
     *
     * @return 缓存服务
     */
    @Bean
    @ConditionalOnMissingBean
    public CacheService cacheService(CacheHelper cacheHelper) {
        if (Objects.isNull(CacheHelper.getRedisTemplate())) {
            return null;
        }
        return CacheHelper.getCache(CacheHelper.getAppName());
    }
}
