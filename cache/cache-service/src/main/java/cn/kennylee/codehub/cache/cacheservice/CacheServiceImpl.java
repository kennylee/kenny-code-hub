package cn.kennylee.codehub.cache.cacheservice;

import cn.hutool.core.util.StrUtil;
import cn.kennylee.codehub.cache.cacheservice.redis.RedisUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * 缓存服务实现类
 * <p>Created on 2025/2/11.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Slf4j
public class CacheServiceImpl implements CacheService {

    /**
     * 是否使用缓存
     */
    @Getter
    @Setter
    private boolean useCache = true;

    /**
     * 缓存根的应用名称
     */
    private final String appName;

    private final RedisUtils redisUtils;

    protected CacheServiceImpl(RedisTemplate<String, Object> redisTemplate) {
        this.appName = null;
        this.redisUtils = RedisUtils.of(redisTemplate);
    }

    protected CacheServiceImpl(String appName, RedisTemplate<String, Object> redisTemplate) {
        this.appName = appName;
        this.redisUtils = RedisUtils.of(this.appName, redisTemplate);
    }

    @Override
    @Nullable
    public String getAppName() {
        return this.appName;
    }

    @Override
    public <T> void setCache(String groupName, String key, T value, long timeout) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置, groupName: {}, key: {}, value: {}, timeout: {}", groupName, key, RedisUtils.toJsonStr(value), timeout);
            }
            return;
        }

        redisUtils.setCache(groupName, key, value, timeout);
    }

    @Override
    public <T> void setCache(String key, T value, long timeout) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置, key: {}, value: {}, timeout: {}", key, RedisUtils.toJsonStr(value), timeout);
            }
            return;
        }

        redisUtils.setCache(key, value, timeout);
    }

    @Override
    public <T> void setCache(String groupName, String key, T value, Duration duration) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置, groupName: {}, key: {}, value: {}, duration: {}", groupName, key, RedisUtils.toJsonStr(value), duration);
            }
            return;
        }

        redisUtils.setCache(groupName, key, value, duration);
    }

    @Override
    public <T> void setCache(String key, T value, Duration duration) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置, key: {}, value: {}, duration: {}", key, RedisUtils.toJsonStr(value), duration);
            }
            return;
        }

        redisUtils.setCache(key, value, duration);
    }

    @Override
    public <T> void setObject(String key, T value, long timeout) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置, key: {}, value: {}, timeout: {}", key, RedisUtils.toJsonStr(value), timeout);
            }
            return;
        }

        redisUtils.setObject(key, value, timeout);
    }

    @Override
    public <T> void setObject(String groupName, String key, T value, Duration duration) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置, groupName: {}, key: {}, value: {}, duration: {}", groupName, key, RedisUtils.toJsonStr(value), duration);
            }
            return;
        }

        redisUtils.setObject(groupName, key, value, duration);
    }

    @Override
    public <T> void setObject(String key, T value, Duration duration) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置, key: {}, value: {}, duration: {}", key, RedisUtils.toJsonStr(value), duration);
            }
            return;
        }
        redisUtils.setObject(key, value, duration);
    }

    @Override
    public Object getCache(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, groupName: {}, key: {}", groupName, key);
            }
            return null;
        }

        return redisUtils.getCache(groupName, key);
    }

    @Override
    public Object getCache(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, key: {}", key);
            }
            return null;
        }

        return redisUtils.getCache(key);
    }

    @Override
    public Optional<Object> getCacheOpt(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, groupName: {}, key: {}", groupName, key);
            }
            return Optional.empty();
        }

        return redisUtils.getCacheOpt(groupName, key);
    }

    @Override
    public Optional<Object> getCacheOpt(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, key: {}", key);
            }
            return Optional.empty();
        }

        return redisUtils.getCacheOpt(key);
    }

    @Override
    public <T> T getObject(String groupName, String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, groupName: {}, key: {}", groupName, key);
            }
            return null;
        }

        return redisUtils.getObject(groupName, key, tClass);
    }

    @Override
    public <T> T getObject(String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, key: {}", key);
            }
            return null;
        }

        return redisUtils.getObject(key, tClass);
    }

    @Override
    public <T> List<T> getList(String groupName, String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, groupName: {}, key: {}", groupName, key);
            }
            return null;
        }

        return redisUtils.getList(groupName, key, tClass);
    }

    @Override
    public <T> List<T> getList(String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, key: {}", key);
            }
            return null;
        }

        return redisUtils.getList(key, tClass);
    }

    @Override
    public <T> Optional<T> getObjectOpt(String groupName, String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, groupName: {}, key: {}", groupName, key);
            }
            return Optional.empty();
        }

        return redisUtils.getObjectOpt(groupName, key, tClass);
    }

    @Override
    public <T> Optional<T> getObjectOpt(String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存获取, key: {}", key);
            }
            return Optional.empty();
        }

        return redisUtils.getObjectOpt(key, tClass);
    }

    @Override
    public <T> T getOrLoadObject(@Nullable String groupName, @NonNull String key, Callable<T> callback, Duration duration, Class<T> tClass) {

        T cacheObj = getObject(groupName, key, tClass);

        if (Objects.nonNull(cacheObj)) {
            return cacheObj;
        }

        try {
            cacheObj = callback.call();

            // TODO 缓存穿透问题

            setObject(groupName, key, cacheObj, duration);
            return cacheObj;
        } catch (Exception e) {
            log.error(StrUtil.format("加载缓存对象失败,groupName: {}, key: {}", groupName, key), e);
            return null;
        }
    }

    @Override
    public <T> T getOrLoadObject(String key, Callable<T> callback, Duration duration, Class<T> tClass) {
        return getOrLoadObject(null, key, callback, duration, tClass);
    }

    @Override
    public <T> List<T> getOrLoadList(String groupName, String key, Callable<List<T>> callback, Duration duration, Class<T> tClass) {
        List<T> cacheList = getList(groupName, key, tClass);

        if (Objects.nonNull(cacheList)) {
            return cacheList;
        }

        try {
            cacheList = callback.call();

            // TODO 缓存穿透问题

            setObject(groupName, key, cacheList, duration);
            return cacheList;
        } catch (Exception e) {
            log.error(StrUtil.format("加载缓存列表失败,groupName: {}, key: {}", groupName, key), e);
            return null;
        }
    }

    @Override
    public <T> List<T> getOrLoadList(String key, Callable<List<T>> callback, Duration duration, Class<T> tClass) {
        return getOrLoadList(null, key, callback, duration, tClass);
    }

    @Override
    public boolean exists(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存判断, groupName: {}, key: {}", groupName, key);
            }
            return false;
        }

        return redisUtils.exists(groupName, key);
    }

    @Override
    public boolean exists(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存判断, key: {}", key);
            }
            return false;
        }

        return redisUtils.exists(key);
    }

    @Override
    public Object deleteAndGet(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, groupName: {}, key: {}", groupName, key);
            }
            return null;
        }

        return redisUtils.deleteAndGet(groupName, key);
    }

    @Override
    public Object deleteAndGet(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, key: {}", key);
            }
            return null;
        }

        return redisUtils.deleteAndGet(key);
    }

    @Override
    public Optional<Object> deleteAndGetOpt(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, groupName: {}, key: {}", groupName, key);
            }
            return Optional.empty();
        }

        return redisUtils.deleteAndGetOpt(groupName, key);
    }

    @Override
    public Optional<Object> deleteAndGetOpt(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, key: {}", key);
            }
            return Optional.empty();
        }

        return redisUtils.deleteAndGetOpt(key);
    }

    @Override
    public <T> T deleteAndGet(String groupName, String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, groupName: {}, key: {}", groupName, key);
            }
            return null;
        }

        return redisUtils.deleteAndGet(groupName, key, tClass);
    }

    @Override
    public <T> T deleteAndGet(String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, key: {}", key);
            }
            return null;
        }

        return redisUtils.deleteAndGet(key, tClass);
    }

    @Override
    public <T> Optional<T> deleteAndGetOpt(String groupName, String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, groupName: {}, key: {}", groupName, key);
            }
            return Optional.empty();
        }

        return redisUtils.deleteAndGetOpt(groupName, key, tClass);
    }

    @Override
    public <T> Optional<T> deleteAndGetOpt(String key, Class<T> tClass) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存删除, key: {}", key);
            }
            return Optional.empty();
        }

        return redisUtils.deleteAndGetOpt(key, tClass);
    }

    @Override
    public boolean expire(String groupName, String key, Duration duration) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置过期时间, groupName: {}, key: {}, duration: {}", groupName, key, duration);
            }
            return false;
        }

        return redisUtils.expire(groupName, key, duration);
    }

    @Override
    public boolean expire(String key, Duration duration) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置过期时间, key: {}, duration: {}", key, duration);
            }
            return false;
        }

        return redisUtils.expire(key, duration);
    }

    @Override
    public boolean expire(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置过期时间, key: {}", key);
            }
            return false;
        }

        return redisUtils.expire(key);
    }

    @Override
    public boolean expire(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过缓存设置过期时间, groupName: {}, key: {}", groupName, key);
            }
            return false;
        }

        return redisUtils.expire(groupName, key);
    }

    @Override
    public boolean addExpireTime(String key, Duration duration) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过增加key的有效时间, key: {}, duration: {}", key, duration);
            }
            return false;
        }

        return redisUtils.addExpireTime(key, duration);
    }

    @Override
    public boolean deleteWithLock(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过删除单个对象, groupName: {}, key: {}", groupName, key);
            }
            return false;
        }

        return redisUtils.deleteWithLock(groupName, key);
    }

    @Override
    public boolean deleteWithLock(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过删除单个对象, key: {}", key);
            }
            return false;
        }

        return redisUtils.deleteWithLock(key);
    }

    @Override
    public boolean delete(String groupName, String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过删除单个对象, groupName: {}, key: {}", groupName, key);
            }
            return false;
        }

        return redisUtils.delete(groupName, key);
    }

    @Override
    public boolean delete(String key) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过删除单个对象, key: {}", key);
            }
            return false;
        }

        return redisUtils.delete(key);
    }

    @Override
    public long delete(String groupName, Collection<String> keys) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过批量删除对象, groupName: {}, keys: {}", groupName, keys);
            }
            return 0;
        }

        return redisUtils.delete(groupName, keys);
    }

    @Override
    public long delete(Collection<String> keys) {
        if (!useCache) {
            if (log.isDebugEnabled()) {
                log.debug("缓存禁用, 跳过批量删除对象, keys: {}", keys);
            }
            return 0;
        }

        return redisUtils.delete(keys);
    }

    @Override
    public Long increment(String groupName, String key, int increment) {
        return redisUtils.increment(groupName, key, increment);
    }

    @Override
    public Long increment(String groupName, String key, int increment, Duration duration) {
        return redisUtils.increment(groupName, key, increment, duration);
    }

    @Override
    public Long increment(String key, int increment) {
        return redisUtils.increment(key, increment);
    }

    @Override
    public boolean checkAndIncrement(String groupName, String key, int limit, Duration duration) {
        return redisUtils.checkAndIncrement(groupName, key, limit, duration);
    }

    @Override
    public boolean checkAndIncrement(String key, int limit, Duration duration) {
        return redisUtils.checkAndIncrement(key, limit, duration);
    }

    @Override
    public Long decrement(String groupName, String key) {
        return redisUtils.decrement(groupName, key);
    }

    @Override
    public Long decrement(String key) {
        return redisUtils.decrement(key);
    }

    @Override
    public String buildCacheKey(String primaryKey) {
        return redisUtils.buildCacheKey(primaryKey);
    }

    @Override
    public String buildCacheKey(String groupName, String primaryKey) {
        return redisUtils.buildCacheKey(groupName, primaryKey);
    }
}
