package cn.kennylee.codehub.cache.cacheservice;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * <p> 缓存服务类 </p>
 * <p>Created on 2025/2/11.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
public interface CacheService {

    /**
     * 获取获取的应用名称
     *
     * @return 应用名称，可能为null，并不设置
     */
    @Nullable
    String getAppName();

    /**
     * 设置序列化缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param value     缓存的值
     * @param timeout   缓存时间，单位秒
     */
    <T> void setCache(@Nullable final String groupName, final String key, final T value, long timeout);

    /**
     * 设置序列化缓存
     *
     * @param key     缓存的键值
     * @param value   缓存的值
     * @param timeout 缓存时间，单位秒
     */
    <T> void setCache(final String key, final T value, long timeout);

    /**
     * 设置序列化缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param value     缓存的值
     * @param duration  缓存时间，null代表永久
     */
    <T> void setCache(@Nullable final String groupName, final String key, final T value, @Nullable final Duration duration);

    <T> void setCache(final String key, final T value, @Nullable final Duration duration);

    /**
     * 设置对象缓存
     *
     * @param key     缓存的键值
     * @param value   缓存的值
     * @param timeout 缓存时间，单位秒
     */
    <T> void setObject(final String key, final T value, long timeout);

    /**
     * 设置对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param value     缓存的值
     * @param duration  缓存时间，null代表永久
     */
    <T> void setObject(@Nullable final String groupName, final String key, final T value, @Nullable final Duration duration);

    <T> void setObject(final String key, final T value, @Nullable final Duration duration);

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    Object getCache(@Nullable final String groupName, final String key);

    Object getCache(final String key);

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @return 缓存的值
     */
    Optional<Object> getCacheOpt(@Nullable final String groupName, final String key);

    Optional<Object> getCacheOpt(final String key);

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @return 缓存的值
     */
    <T> T getObject(@Nullable final String groupName, final String key, final Class<T> tClass);

    <T> T getObject(final String key, final Class<T> tClass);

    /**
     * 获取对象缓存列表
     *
     * @param groupName 缓存组名，可为null，为null时不加入组名
     * @param key       缓存的键值
     * @param tClass    缓存的值类型
     * @param <T>       缓存的值类型
     * @return 缓存对应对象列表，如果不存在则返回null
     */
    @Nullable
    <T> List<T> getList(@Nullable final String groupName, final String key, final Class<T> tClass);

    /**
     * 获取对象缓存列表
     *
     * @param key    缓存的键值
     * @param tClass 缓存的值类型
     * @param <T>    缓存的值类型
     * @return 缓存对应对象列表，如果不存在则返回null
     */
    @Nullable
    <T> List<T> getList(final String key, final Class<T> tClass);

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    <T> Optional<T> getObjectOpt(@Nullable final String groupName, final String key, final Class<T> tClass);

    <T> Optional<T> getObjectOpt(final String key, final Class<T> tClass);

    /**
     * 获取对象缓存，如果不存在则加载
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param tClass    缓存的值类型
     * @param duration  缓存时间，null代表永久
     * @param callback  如果缓存没命中，执行加载的逻辑回调方法
     * @param <T>       缓存的值
     * @return 缓存的值
     */
    @Nullable
    <T> T getOrLoadObject(@Nullable final String groupName, final String key, Callable<T> callback, @Nullable Duration duration, final Class<T> tClass);

    /**
     * 获取对象缓存，如果不存在则加载
     *
     * @param key      缓存的键值
     * @param tClass   缓存的值类型
     * @param callback 如果缓存没命中，执行加载的逻辑回调方法
     * @param duration 缓存时间，null代表永久
     * @param <T>      缓存的值
     * @return 缓存的值
     */
    @Nullable
    <T> T getOrLoadObject(final String key, Callable<T> callback, @Nullable Duration duration, final Class<T> tClass);

    /**
     * 获取对象缓存，如果不存在则加载
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param tClass    缓存的值类型
     * @param duration  缓存时间，null代表永久
     * @param callback  如果缓存没命中，执行加载的逻辑回调方法
     * @param <T>       缓存的值
     * @return 缓存的值
     */
    @Nullable
    <T> List<T> getOrLoadList(@Nullable final String groupName, final String key, Callable<List<T>> callback, @Nullable Duration duration, final Class<T> tClass);

    /**
     * 获取对象缓存，如果不存在则加载
     *
     * @param key      缓存的键值
     * @param tClass   缓存的值类型
     * @param callback 如果缓存没命中，执行加载的逻辑回调方法
     * @param duration 缓存时间，null代表永久
     * @param <T>      缓存的值
     * @return 缓存的值
     */
    @Nullable
    <T> List<T> getOrLoadList(final String key, Callable<List<T>> callback, @Nullable Duration duration, final Class<T> tClass);

    /**
     * 缓存key是否存在
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    boolean exists(@Nullable final String groupName, final String key);

    boolean exists(final String key);

    /**
     * 删除key并返回内容，如果有的话
     * <p>线程安全</p>
     *
     * @param key 缓存的键值
     * @return 缓存的值
     */
    @Nullable
    Object deleteAndGet(@Nullable final String groupName, final String key);

    @Nullable
    Object deleteAndGet(final String key);

    Optional<Object> deleteAndGetOpt(@Nullable final String groupName, final String key);

    Optional<Object> deleteAndGetOpt(final String key);

    /**
     * 删除key并返回内容，如果有的话
     * <p>线程安全</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @return 缓存的值
     */
    @Nullable
    <T> T deleteAndGet(@Nullable final String groupName, final String key, final Class<T> tClass);

    @Nullable
    <T> T deleteAndGet(final String key, final Class<T> tClass);

    <T> Optional<T> deleteAndGetOpt(@Nullable final String groupName, final String key, final Class<T> tClass);

    <T> Optional<T> deleteAndGetOpt(final String key, final Class<T> tClass);

    /**
     * <p>设置key的有效时间，如果存在的话</p>
     * <p>如果key不存在，返回false</p>
     *
     * @param key      缓存的键值
     * @param duration 缓存时间，null代表永久
     * @return true=设置成功；false=设置失败
     */
    boolean expire(@Nullable final String groupName, final String key, @Nullable final Duration duration);

    boolean expire(final String key, @Nullable final Duration duration);

    /**
     * <p>设置key永久有效，如果存在的话</p>
     * <p>如果key不存在，返回false</p>
     *
     * @param key 缓存的键值
     * @return true=设置成功；false=设置失败
     */
    boolean expire(final String key);

    boolean expire(@Nullable final String groupName, final String key);

    /**
     * <p>增加key的有效时间（在原来有效期范围上延迟），如果存在的话</p>
     * <p>如果key不存在，返回false</p>
     *
     * @param key      缓存的键值
     * @param duration 需要延长的时间，null代表永久
     * @return true = 延长成功；false = 延长失败，不存在或者无限期
     */
    boolean addExpireTime(final String key, @Nullable final Duration duration);

    /**
     * <p>删除单个对象</p>
     * <p>线程安全</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @return 是否删除成功
     */
    boolean deleteWithLock(@Nullable final String groupName, final String key);

    /**
     * <p>删除单个对象</p>
     * <p>线程安全</p>
     *
     * @param key 缓存键值
     * @return 是否删除成功
     */
    boolean deleteWithLock(final String key);

    /**
     * <p>删除单个对象</p>
     * <p>非线程安全</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @return 是否删除成功
     */
    boolean delete(@Nullable final String groupName, final String key);

    boolean delete(final String key);

    /**
     * <p>批量删除对象</p>
     * <p>非线程安全</p>
     *
     * @param keys 多个对象
     * @return 删除的数量
     */
    long delete(@Nullable final String groupName, final Collection<String> keys);

    long delete(final Collection<String> keys);

    /**
     * 根据key和给定的增量进行递增/减少
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @param increment 增量
     * @return 递增/减少后的值
     */
    Long increment(@Nullable final String groupName, String key, int increment);

    /**
     * 根据key和给定的增量进行递增/减少
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @param increment 增量
     * @param duration  过期时间
     * @return 递增/减少后的值
     */
    Long increment(@Nullable final String groupName, String key, int increment, @Nullable Duration duration);

    /**
     * 根据key和给定的增量进行递增/减少
     *
     * @param key       缓存键值
     * @param increment 增量(递增为正数，递减为负数)
     * @return 递增/减少后的值
     */
    Long increment(String key, int increment);

    /**
     * <p>检查并递增计数器</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @param limit     最大限制值，超过则返回 false
     * @param duration  有效时间
     * @return 是否超出限制
     */
    boolean checkAndIncrement(@Nullable final String groupName, @NonNull String key, int limit, @Nullable Duration duration);

    /**
     * <p>检查并递增计数器</p>
     *
     * @param key      缓存键值
     * @param limit    限制
     * @param duration 有效时间
     * @return 是否超出限制
     */
    boolean checkAndIncrement(@NonNull String key, int limit, @Nullable Duration duration);

    /**
     * <p>递减计数器</p>
     * <p>一般用在操作Increment后，业务失败的回退情景</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @return 递减后的值
     */
    Long decrement(@Nullable final String groupName, String key);

    /**
     * <p>递减计数器</p>
     * <p>一般用在操作Increment后，业务失败的回退情景</p>
     *
     * @param key 缓存键值
     * @return 递减后的值
     */
    Long decrement(String key);

    /**
     * 构建缓存key
     *
     * @param primaryKey 缓存key主名字
     * @return 缓存key
     */
    String buildCacheKey(String primaryKey);

    /**
     * <p>构建缓存key</p>
     *
     * @param groupName  缓存组名
     * @param primaryKey 锁的主键
     * @return 缓存key
     */
    String buildCacheKey(@Nullable String groupName, String primaryKey);
}
