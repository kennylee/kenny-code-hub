package cn.kennylee.codehub.cache.cacheservice;

import cn.hutool.extra.spring.SpringUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 缓存工作类
 * <p>Created on 2025/2/11.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Component
@Slf4j
public class CacheHelper {

    public static final String DEFAULT_CACHE = "default";

    /**
     * 缓存服务Map
     */
    private static final Map<String, CacheService> CACHE_SERVICE_MAP = new HashMap<>();

    @Getter
    @Setter
    private static RedisTemplate<String, Object> redisTemplate;

    public CacheHelper(@Autowired RedisTemplate<String, Object> redisTemplate) {
        CacheHelper.redisTemplate = redisTemplate;
    }

    /**
     * 获取缓存服务，不使用根应用名称标识
     * <p>如果redisTemplate为空，返回null</p>
     *
     * @return 缓存服务
     */
    @Nullable
    public static CacheService getCache() {
        return getCache(DEFAULT_CACHE);
    }

    /**
     * 获取缓存服务
     * <p>如果redisTemplate为空，返回null</p>
     *
     * @param appName 缓存根的应用名称标识
     * @return 缓存服务
     */
    @Nullable
    public static CacheService getCache(String appName) {
        if (Objects.isNull(redisTemplate)) {
            log.error("RedisTemplate为空, 获取缓存服务失败");
            return null;
        }
        return CACHE_SERVICE_MAP.computeIfAbsent(appName, key -> {
            if (DEFAULT_CACHE.equals(key)) {
                // 默认缓存服务，使用无app标识的全局名作根
                return new CacheServiceImpl(redisTemplate);
            } else {
                return new CacheServiceImpl(key, redisTemplate);
            }
        });
    }

    /**
     * 获取应用名称
     *
     * @return 应用名称
     */
    @Nullable
    public static String getAppName() {
        return SpringUtil.getApplicationContext().getEnvironment().getProperty("spring.application.name");
    }
}
