package cn.kennylee.codehub.cache.cacheservice.redis;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONException;
import cn.hutool.json.JSONUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Redis缓存工具类
 * <p>Created on 2024/12/27.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Slf4j
public class RedisUtils {

    private final RedisTemplate<String, Object> redisTemplate;

    @Getter
    private final String appName;

    /**
     * 构造函数
     *
     * @param baseName      缓存根的应用名称
     * @param redisTemplate RedisTemplate
     */
    public RedisUtils(String baseName, RedisTemplate<String, Object> redisTemplate) {
        this.appName = baseName;
        this.redisTemplate = redisTemplate;
    }

    /**
     * 构造函数
     *
     * @param redisTemplate RedisTemplate
     */
    public RedisUtils(RedisTemplate<String, Object> redisTemplate) {
        this.appName = null;
        this.redisTemplate = redisTemplate;
    }

    /**
     * 静态沟通函数
     */
    public static RedisUtils of(RedisTemplate<String, Object> redisTemplate) {
        return new RedisUtils(redisTemplate);
    }

    /**
     * 静态沟通函数
     */
    public static RedisUtils of(String baseName, RedisTemplate<String, Object> redisTemplate) {
        return new RedisUtils(baseName, redisTemplate);
    }

    /**
     * 设置序列化缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param value     缓存的值
     * @param timeout   缓存时间，单位秒
     */
    public <T> void setCache(@Nullable final String groupName, final String key, final T value, long timeout) {
        setCache(groupName, key, value, Duration.ofSeconds(timeout));
    }

    /**
     * 设置序列化缓存
     *
     * @param key     缓存的键值
     * @param value   缓存的值
     * @param timeout 缓存时间，单位秒
     */
    public <T> void setCache(final String key, final T value, long timeout) {
        setCache(null, key, value, timeout);
    }

    /**
     * 设置序列化缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param value     缓存的值
     * @param duration  缓存时间，null代表永久
     */
    public <T> void setCache(@Nullable final String groupName, final String key, final T value, @Nullable final Duration duration) {
        String finalKey = buildCacheKey(groupName, key);
        if (Objects.isNull(duration)) {
            if (log.isDebugEnabled()) {
                log.debug("设置缓存：key={}, value={}", finalKey, toJsonStr(value));
            }

            if (Objects.isNull(value)) {
                log.warn("设置缓存失败，value为空，key={}", finalKey);
                return;
            }

            redisTemplate.opsForValue().set(finalKey, value);
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("设置缓存：key={}, value={}, duration={}", finalKey, toJsonStr(value), duration);
        }

        if (Objects.isNull(value)) {
            log.warn("设置缓存失败，value为空，key={}", finalKey);
            return;
        }

        redisTemplate.opsForValue().set(finalKey, value, duration);
    }

    public <T> void setCache(final String key, final T value, @Nullable final Duration duration) {
        setCache(null, key, value, duration);
    }

    /**
     * 设置对象缓存
     *
     * @param key     缓存的键值
     * @param value   缓存的值
     * @param timeout 缓存时间，单位秒
     */
    public <T> void setObject(final String key, final T value, long timeout) {
        setObject(key, value, Duration.ofSeconds(timeout));
    }

    /**
     * 设置对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @param value     缓存的值
     * @param duration  缓存时间，null代表永久
     */
    public <T> void setObject(@Nullable final String groupName, final String key, final T value, @Nullable final Duration duration) {
        if (Objects.isNull(duration)) {
            if (Objects.isNull(value)) {
                log.warn("设置缓存失败，value为空，key={}", buildCacheKey(groupName, key));
                return;
            }

            redisTemplate.opsForValue().set(buildCacheKey(groupName, key), toJsonStr(value));
            if (log.isDebugEnabled()) {
                log.debug("设置缓存：key={}, value={}", buildCacheKey(groupName, key), toJsonStr(value));
            }
            return;
        }
        this.setCache(groupName, key, toJsonStr(value), duration);
    }

    public <T> void setObject(final String key, final T value, @Nullable final Duration duration) {
        setObject(null, key, value, duration);
    }

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    public Object getCache(@Nullable final String groupName, final String key) {
        Object cacheObj = redisTemplate.opsForValue().get(buildCacheKey(groupName, key));
        if (Objects.nonNull(cacheObj)) {
            if (log.isDebugEnabled()) {
                log.debug("命中缓存：key={}", buildCacheKey(groupName, key));
            }
        }
        return cacheObj;
    }

    public Object getCache(final String key) {
        return getCache(null, key);
    }

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    public Optional<Object> getCacheOpt(@Nullable final String groupName, final String key) {
        return Optional.ofNullable(getCache(groupName, key));
    }

    public Optional<Object> getCacheOpt(final String key) {
        return getCacheOpt(null, key);
    }

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    public <T> T getObject(@Nullable final String groupName, final String key, final Class<T> tClass) {
        Object val = redisTemplate.opsForValue().get(buildCacheKey(groupName, key));
        if (Objects.isNull(val)) {
            return null;
        }
        if (log.isDebugEnabled()) {
            log.debug("命中缓存：key={}", buildCacheKey(groupName, key));
        }
        return jsonObjToBean(val, tClass);
    }

    public <T> T getObject(final String key, final Class<T> tClass) {
        return getObject(null, key, tClass);
    }

    /**
     * 获取对象缓存列表
     *
     * @param groupName 缓存组名，可为null，为null时不加入组名
     * @param key       缓存的键值
     * @param tClass    缓存的值类型
     * @param <T>       缓存的值类型
     * @return 缓存对应对象列表，如果不存在则返回null
     */
    @Nullable
    public <T> List<T> getList(@Nullable final String groupName, final String key, final Class<T> tClass) {
        Object val = redisTemplate.opsForValue().get(buildCacheKey(groupName, key));
        if (Objects.isNull(val)) {
            return null;
        }
        if (log.isDebugEnabled()) {
            log.debug("命中缓存：key={}", buildCacheKey(groupName, key));
        }
        return jsonObjToList(val, tClass);
    }

    /**
     * 获取对象缓存列表
     *
     * @param key    缓存的键值
     * @param tClass 缓存的值类型
     * @param <T>    缓存的值类型
     * @return 缓存对应对象列表，如果不存在则返回null
     */
    @Nullable
    public <T> List<T> getList(final String key, final Class<T> tClass) {
        return getList(null, key, tClass);
    }

    /**
     * 获取对象缓存
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    public <T> Optional<T> getObjectOpt(@Nullable final String groupName, final String key, final Class<T> tClass) {
        return Optional.ofNullable(getObject(groupName, key, tClass));
    }

    public <T> Optional<T> getObjectOpt(final String key, final Class<T> tClass) {
        return getObjectOpt(null, key, tClass);
    }

    /**
     * 缓存key是否存在
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     */
    public boolean exists(@Nullable final String groupName, final String key) {
        return redisTemplate.hasKey(buildCacheKey(groupName, key));
    }

    public boolean exists(final String key) {
        return exists(null, key);
    }

    /**
     * 删除key并返回内容，如果有的话
     * <p>线程安全</p>
     *
     * @param key 缓存的键值
     * @return 缓存的值
     */
    @Nullable
    public Object deleteAndGet(@Nullable final String groupName, final String key) {
        Object value = getCache(groupName, key);
        if (Objects.isNull(value)) {
            return null;
        }
        if (this.deleteWithLock(groupName, key)) {
            return value;
        }
        log.warn("删除缓存失败，key={}", key);
        return null;
    }

    @Nullable
    public Object deleteAndGet(final String key) {
        return deleteAndGet(null, key);
    }

    public Optional<Object> deleteAndGetOpt(@Nullable final String groupName, final String key) {
        return Optional.ofNullable(deleteAndGet(groupName, key));
    }

    public Optional<Object> deleteAndGetOpt(final String key) {
        return deleteAndGetOpt(null, key);
    }

    /**
     * 删除key并返回内容，如果有的话
     * <p>线程安全</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存的键值
     * @return 缓存的值
     */
    @Nullable
    public <T> T deleteAndGet(@Nullable final String groupName, final String key, final Class<T> tClass) {
        Object value = deleteAndGet(groupName, key);
        if (Objects.isNull(value)) {
            return null;
        }
        return jsonObjToBean(value, tClass);
    }

    @Nullable
    public <T> T deleteAndGet(final String key, final Class<T> tClass) {
        return deleteAndGet(null, key, tClass);
    }

    public <T> Optional<T> deleteAndGetOpt(@Nullable final String groupName, final String key, final Class<T> tClass) {
        return Optional.ofNullable(deleteAndGet(groupName, key, tClass));
    }

    public <T> Optional<T> deleteAndGetOpt(final String key, final Class<T> tClass) {
        return deleteAndGetOpt(null, key, tClass);
    }

    /**
     * <p>设置key的有效时间，如果存在的话</p>
     * <p>如果key不存在，返回false</p>
     *
     * @param key      缓存的键值
     * @param duration 缓存时间，null代表永久
     * @return true=设置成功；false=设置失败
     */
    public boolean expire(@Nullable final String groupName, final String key, @Nullable final Duration duration) {
        if (duration == null) {
            return redisTemplate.persist(buildCacheKey(groupName, key));
        }
        return Boolean.TRUE.equals(redisTemplate.expire(buildCacheKey(groupName, key), duration));
    }

    public boolean expire(final String key, @Nullable final Duration duration) {
        return expire(null, key, duration);
    }

    /**
     * <p>设置key永久有效，如果存在的话</p>
     * <p>如果key不存在，返回false</p>
     *
     * @param key 缓存的键值
     * @return true=设置成功；false=设置失败
     */
    public boolean expire(final String key) {
        return expire(null, key, null);
    }

    public boolean expire(@Nullable final String groupName, final String key) {
        return expire(groupName, key, null);
    }

    /**
     * <p>增加key的有效时间（在原来有效期范围上延迟），如果存在的话</p>
     * <p>如果key不存在，返回false</p>
     *
     * @param key      缓存的键值
     * @param duration 需要延长的时间，null代表永久
     * @return true = 延长成功；false = 延长失败，不存在或者无限期
     */
    public boolean addExpireTime(final String key, @Nullable final Duration duration) {
        // 获取当前剩余的过期时间（单位是秒）
        long currentExpireTime = redisTemplate.getExpire(buildCacheKey(key), TimeUnit.MILLISECONDS);

        // 如果当前过期时间为-1，说明键没有设置过期时间，-2代表不存在
        if (currentExpireTime < 0) {
            return false;
        }

        if (Objects.isNull(duration)) {
            return expire(key);
        }

        // 计算新的过期时间
        long newExpireTime = currentExpireTime + duration.toMillis();

        // 重新设置新的过期时间
        redisTemplate.expireAt(buildCacheKey(key), Instant.now().plusMillis(newExpireTime));

        return true;
    }

    /**
     * <p>删除单个对象</p>
     * <p>线程安全</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @return 是否删除成功
     */
    public boolean deleteWithLock(@Nullable final String groupName, final String key) {
        final String finalKey = buildCacheKey(groupName, key);
        final String deleteLock = finalKey + "_lock";
        // 设置一个“删除标志”键。使用 SETNX（SET if Not Exists）来实现
        Boolean lockAcquired = redisTemplate.opsForValue().setIfAbsent(deleteLock, "locked", Duration.ofSeconds(5));

        if (Boolean.FALSE.equals(lockAcquired)) {
            log.warn("删除缓存失败，key={}，已被锁定", key);
            return false;
        }

        try {
            return redisTemplate.delete(finalKey);
        } finally {
            redisTemplate.delete(deleteLock);
        }
    }

    /**
     * <p>删除单个对象</p>
     * <p>线程安全</p>
     *
     * @param key 缓存键值
     * @return 是否删除成功
     */
    public boolean deleteWithLock(final String key) {
        return deleteWithLock(null, key);
    }

    /**
     * <p>删除单个对象</p>
     * <p>非线程安全</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @return 是否删除成功
     */
    public boolean delete(@Nullable final String groupName, final String key) {
        return redisTemplate.delete(buildCacheKey(groupName, key));
    }

    public boolean delete(final String key) {
        return delete(null, key);
    }

    /**
     * <p>批量删除对象</p>
     * <p>非线程安全</p>
     *
     * @param keys 多个对象
     * @return 删除的数量
     */
    public long delete(@Nullable final String groupName, final Collection<String> keys) {
        return redisTemplate.delete(keys.stream().map(key -> buildCacheKey(groupName, key)).collect(Collectors.toList()));
    }

    public long delete(final Collection<String> keys) {
        return delete(null, keys);
    }

    /**
     * 根据key和给定的增量进行递增/减少
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @param increment 增量
     * @return 递增/减少后的值
     */
    public Long increment(@Nullable final String groupName, String key, int increment) {
        return increment(groupName, key, increment, null);
    }

    /**
     * 根据key和给定的增量进行递增/减少
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @param increment 增量
     * @param duration  过期时间
     * @return 递增/减少后的值
     */
    public Long increment(@Nullable final String groupName, String key, int increment,
                          @Nullable Duration duration) {

        if (Objects.nonNull(duration)) {
            // 使用 SETNX 命令判断 key 是否存在，如果不存在则设置过期时间
            Boolean exists = redisTemplate.opsForValue().setIfAbsent(buildCacheKey(groupName, key), 0);
            // 第一次才会设置成
            if (Boolean.TRUE.equals(exists)) {
                // 设置过期时间
                this.expire(groupName, key, duration);
            }
        }

        return redisTemplate.opsForValue().increment(buildCacheKey(groupName, key), increment);
    }

    /**
     * 根据key和给定的增量进行递增/减少
     *
     * @param key       缓存键值
     * @param increment 增量(递增为正数，递减为负数)
     * @return 递增/减少后的值
     */
    public Long increment(String key, int increment) {
        return increment(null, key, increment);
    }

    /**
     * <p>检查并递增计数器</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @param limit     最大限制值，超过则返回 false
     * @param duration  有效时间
     * @return 是否超出限制
     */
    public boolean checkAndIncrement(@Nullable final String groupName, @NonNull String key, int limit, @Nullable Duration duration) {
        if (limit <= 0) {
            throw new IllegalArgumentException("limit要是正整数");
        }
        // 使用 Redis 的 INCR 操作递增计数器，返回递增后的新值
        Long count = this.increment(groupName, key, 1, duration);

        if (log.isDebugEnabled()) {
            log.debug("检查并递增计数器：key={}, count={}", buildCacheKey(groupName, key), count);
        }

        // 检查计数器值是否超出限制，如果超过则返回 false
        return Objects.nonNull(count) && count <= limit;
    }

    /**
     * <p>检查并递增计数器</p>
     *
     * @param key      缓存键值
     * @param limit    限制
     * @param duration 有效时间
     * @return 是否超出限制
     */
    public boolean checkAndIncrement(@NonNull String key, int limit, @Nullable Duration duration) {
        return checkAndIncrement(null, key, limit, duration);
    }

    /**
     * <p>递减计数器</p>
     * <p>一般用在操作Increment后，业务失败的回退情景</p>
     *
     * @param groupName 缓存组名
     * @param key       缓存键值
     * @return 递减后的值
     */
    public Long decrement(@Nullable final String groupName, String key) {
        return redisTemplate.opsForValue().decrement(buildCacheKey(groupName, key));
    }

    public Long decrement(String key) {
        return decrement(null, key);
    }

    /**
     * 构建缓存key
     *
     * @param primaryKey 缓存key主名字
     * @return 缓存key
     */
    public String buildCacheKey(String primaryKey) {
        return buildCacheKey(null, primaryKey);
    }

    /**
     * <p>构建缓存key</p>
     *
     * @param groupName  缓存组名
     * @param primaryKey 锁的主键
     * @return 缓存key
     */
    public String buildCacheKey(@Nullable String groupName, String primaryKey) {
        String appName = this.getAppName();
        final List<String> keyItems = CollUtil.newArrayList();
        keyItems.add(appName);
        keyItems.add(groupName);
        keyItems.add(primaryKey);
        return StrUtil.join(":", keyItems.stream().filter(StrUtil::isNotBlank).collect(Collectors.toList()));
    }

    private static <T> T jsonObjToBean(Object value, Class<T> tClass) {
        return JSONUtil.toBean(value.toString(), tClass);
    }

    private static <T> List<T> jsonObjToList(Object value, Class<T> tClass) {
        String json = value.toString();
        if (false == JSONUtil.isTypeJSONArray(json)) {
            log.warn("json数据不是数组，json={}", json);
            return Collections.emptyList();
        }
        try {
            return JSONUtil.toList(json, tClass);
        } catch (JSONException e) {
            log.error(StrUtil.format("解析JSON失败, 原始内容是: {}", tClass), e);
            return Collections.emptyList();
        }
    }

    public static <T> String toJsonStr(T value) {
        return JSONUtil.toJsonStr(value, JSONConfig.create().setIgnoreNullValue(true));
    }

}
