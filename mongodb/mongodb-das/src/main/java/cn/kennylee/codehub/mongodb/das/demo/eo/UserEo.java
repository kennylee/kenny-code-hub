package cn.kennylee.codehub.mongodb.das.demo.eo;

import cn.kennylee.codehub.mongodb.das.entity.MongoDbEo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

/**
 * <p> 用户对象 </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Document(collection = "users")
@CompoundIndexes({
    // 唯一索引
    @CompoundIndex(name = "idx_user_name", def = "{'userName': 1}, { 'unique': true }"),
})
@Getter
@Setter
public class UserEo implements MongoDbEo<Long>, Cloneable {

    @Id
    private Long id;
    /**
     * 用户名
     */
    private String userName;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 手机号码
     */
    private Long mobile;

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof UserEo userEo)) {
            return false;
        }
        return Objects.equals(id, userEo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public UserEo clone() {
        try {
            return (UserEo) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
