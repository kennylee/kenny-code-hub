package cn.kennylee.codehub.mongodb.das.extension;

import org.springframework.data.mongodb.core.query.Query;

/**
 * <p> Query查询条件逻辑扩展 </p>
 * <p>Created on 2024/12/10.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@FunctionalInterface
public interface ExtQueryConsumerFunc {
    /**
     * 接收Query，实现扩展逻辑
     *
     * @param query 查询条件
     */
    void accept(Query query);
}
