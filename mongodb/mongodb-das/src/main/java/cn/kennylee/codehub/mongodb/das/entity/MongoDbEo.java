package cn.kennylee.codehub.mongodb.das.entity;

import java.io.Serializable;

/**
 * <p> MongoDB eo对象 </p>
 * <p>Created on 2024/12/6.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
public interface MongoDbEo<P extends Serializable> {

    /**
     * 获取主键ID
     *
     * @return 主键ID
     */
    P getId();

    /**
     * 设置主键ID
     *
     * @param id 主键ID
     */
    void setId(P id);
}
