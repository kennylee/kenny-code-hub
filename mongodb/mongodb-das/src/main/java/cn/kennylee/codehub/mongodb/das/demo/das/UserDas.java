package cn.kennylee.codehub.mongodb.das.demo.das;

import cn.kennylee.codehub.mongodb.das.demo.eo.UserEo;
import cn.kennylee.codehub.mongodb.das.extension.AbstractMongoDas;
import org.springframework.stereotype.Service;

/**
 * <p> 用户数据访问服务 </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Service
public class UserDas extends AbstractMongoDas<UserEo, Long> {
}
