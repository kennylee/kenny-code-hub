package cn.kennylee.codehub.mongodb.das;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongodbDasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongodbDasApplication.class, args);
	}

}
