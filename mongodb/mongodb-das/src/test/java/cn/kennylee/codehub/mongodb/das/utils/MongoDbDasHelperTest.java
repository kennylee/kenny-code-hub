package cn.kennylee.codehub.mongodb.das.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <p> MongoDbDasHelper单元测试 </p>
 * <p>Created on 2025/2/7.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
class MongoDbDasHelperTest {

    @Test
    @DisplayName("测试主键字段名")
    void testIdFieldName() {
        assertEquals("_id", MongoDbDasHelper.MONGO_DB_ID_NAME);
        assertEquals("id", MongoDbDasHelper.PK_FIELD_NAME);
    }
}
