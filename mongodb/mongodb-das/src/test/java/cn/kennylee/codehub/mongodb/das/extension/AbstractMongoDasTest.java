package cn.kennylee.codehub.mongodb.das.extension;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import cn.kennylee.codehub.common.das.dto.CustomPageDto;
import cn.kennylee.codehub.mongodb.das.demo.das.UserDas;
import cn.kennylee.codehub.mongodb.das.demo.dto.UserPageDto;
import cn.kennylee.codehub.mongodb.das.demo.eo.UserEo;
import cn.kennylee.codehub.mongodb.das.test.config.EmbeddedMongoConfig;
import cn.kennylee.codehub.mongodb.das.utils.MongoDbDasHelper;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.mongodb.BulkOperationException;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.test.context.ContextConfiguration;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p> AbstractMongoDas单元测试 </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
// 只会加载 MongoDB 相关组件，Spring Boot 会自动启用 flapdoodle.embed.mongo
@DataMongoTest
// springboot的其他配置，例如bean扫描路径
@ContextConfiguration(classes = EmbeddedMongoConfig.class)
@Slf4j
class AbstractMongoDasTest {

    @Resource
    private UserDas userDas;

    @Resource
    private MongoTemplate mongoTemplate;

    @Test
    @DisplayName("测试分页查询 -- 冒烟")
    void testFindPage() {
        // given
        var users = batchSaveTestData();

        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setUserNameIn(users.stream().map(UserEo::getUserName).collect(Collectors.toSet()));

        // when
        CustomPageDto<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        log.info("分页查询结果: \n{}", JSONUtil.toJsonPrettyStr(userEoPage.getData()));
        Assertions.assertEquals(2, userEoPage.getData().size());
        Assertions.assertEquals(2, userEoPage.getTotal());
    }

    @Test
    @DisplayName("测试分页查询 -- 模糊查询")
    void testFindPage_2() {
        // given
        var user1 = new UserEo();
        user1.setId(IdUtil.getSnowflakeNextId());
        user1.setUserName("testUser" + RandomUtil.randomString(7));
        user1.setEmail(user1.getUserName() + "@example.com");
        userDas.save(user1);

        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setUserNameLike(user1.getUserName().substring(2, 10));

        // when
        CustomPageDto<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        Assertions.assertEquals(1, userEoPage.getData().size());
        Assertions.assertEquals(1, userEoPage.getTotal());
        Assertions.assertEquals(user1.getId(), userEoPage.getData().get(0).getId());
    }

    @Test
    @DisplayName("测试分页查询 -- 后模糊查询")
    void testFindPage_3() {
        // given
        var user1 = new UserEo();
        user1.setId(IdUtil.getSnowflakeNextId());
        user1.setUserName("testUser" + RandomUtil.randomString(7));
        user1.setEmail(user1.getUserName() + "@example.com");
        userDas.save(user1);

        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setUserNameLike(user1.getUserName().substring(0, 10));

        // when
        CustomPageDto<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        Assertions.assertEquals(1, userEoPage.getData().size());
        Assertions.assertEquals(1, userEoPage.getTotal());
        Assertions.assertEquals(user1.getId(), userEoPage.getData().get(0).getId());
    }

    @Test
    @DisplayName("测试分页查询 -- 前模糊查询")
    void testFindPage_4() {
        // given
        var user1 = new UserEo();
        user1.setId(IdUtil.getSnowflakeNextId());
        user1.setUserName("testUser" + RandomUtil.randomString(7));
        user1.setEmail(user1.getUserName() + "@example.com");
        userDas.save(user1);

        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setUserNamePreLike(user1.getUserName().substring(5));

        // when
        CustomPageDto<UserEo> userEoPage = userDas.findPage(userPageDto);

        // then
        Assertions.assertNotNull(userEoPage);
        Assertions.assertEquals(1, userEoPage.getData().size());
        Assertions.assertEquals(1, userEoPage.getTotal());
        Assertions.assertEquals(user1.getId(), userEoPage.getData().get(0).getId());
    }

    @Test
    @DisplayName("测试分页查询 -- 数字不能模糊查询")
    void testFindPage_5() {
        // given
        var user1 = new UserEo();
        user1.setId(IdUtil.getSnowflakeNextId());
        user1.setUserName("testUser" + RandomUtil.randomString(7));
        user1.setEmail(user1.getUserName() + "@example.com");
        user1.setMobile(12345678901L);
        userDas.save(user1);

        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setMobileLike(1L);

        // when
        var illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> userDas.findPage(userPageDto));

        // then
        Assertions.assertNotNull(illegalArgumentException);
        log.info(illegalArgumentException.getMessage(), illegalArgumentException);
    }

    @Test
    @DisplayName("测试根据实例查询所有，返回流 -- 冒烟")
    void testFindAllStreamByExample() {
        // given
        List<UserEo> userEos = batchSaveTestData(RandomUtil.randomInt(10, 21));
        UserPageDto userPageDto = new UserPageDto();
        userPageDto.setUserNameIn(userEos.stream().map(UserEo::getUserName).collect(Collectors.toSet()));

        // when
        List<UserEo> persistedUserEos = CollUtil.newArrayList();
        try (Stream<UserEo> userEoStream = userDas.findAllStreamByExample(userPageDto)) {
            userEoStream.forEach(persistedUserEos::add);
        }

        // then
        Assertions.assertEquals(userEos.size(), persistedUserEos.size());
        // 全匹配
        Assertions.assertTrue(userEos.containsAll(persistedUserEos));
    }

    @Test
    @DisplayName("测试批量保存 -- 冒烟")
    void testSaveBatch() {
        // given
        List<UserEo> userEos = generateTestUserEos();

        // when
        int count = userDas.saveBatch(userEos);

        // then
        Assertions.assertEquals(2, count);
    }

    @Test
    @DisplayName("测试批量保存 -- 主键重复报错")
    void testSaveBatch_2() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        userEos.add(userEos.get(0));

        // when
        var exception = Assertions.assertThrows(DuplicateKeyException.class, () -> userDas.saveBatch(userEos));

        // then
        Assertions.assertNotNull(exception);
        log.info(exception.getMessage(), exception);
    }

    @Test
    @DisplayName("测试批量保存 -- 主键为null")
    void testSaveBatch_3() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        UserEo userEoCopy = userEos.get(0).clone();
        userEoCopy.setId(null);
        userEos.add(userEoCopy);

        // when
        log.info("测试数据: {}", JSONUtil.toJsonPrettyStr(userEos));
        Assertions.assertEquals(3, userEos.size());
        var exception = Assertions.assertThrows(InvalidDataAccessApiUsageException.class, () -> userDas.saveBatch(userEos));

        // then
        Assertions.assertNotNull(exception);
        log.info(exception.getMessage(), exception);
    }

    @Test
    @DisplayName("测试批量保存 -- 报错数据回滚")
    void testSaveBatch_rollback() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        userEos.add(userEos.get(0));

        // when
        var exception = Assertions.assertThrows(DuplicateKeyException.class, () -> userDas.saveBatch(userEos));

        // then
        Collection<Long> ids = userEos.stream().map(UserEo::getId).toList();
        Assertions.assertEquals(0, userDas.countByIds(ids));
    }

    @Test
    @DisplayName("测试批量保存 -- 报错数据不回滚")
    void testSaveBatch_notRollback() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        userEos.add(userEos.get(0));

        // when
        var exception = Assertions.assertThrows(DuplicateKeyException.class, () -> userDas.saveBatch(userEos, false));

        // then
        Collection<Long> ids = userEos.stream().map(UserEo::getId).toList();
        Assertions.assertEquals(2, userDas.countByIds(ids));
    }

    @Test
    @DisplayName("测试批量保存，分片插入")
    void testSaveBatch_split() {
        // given
        List<UserEo> userEos = generateTestUserEos(RandomUtil.randomInt(10, 21));

        // when
        var count = userDas.saveBatch(userEos, 1);

        // then
        Assertions.assertEquals(userEos.size(), count);
    }

    @Test
    @DisplayName("测试批量保存，分片插入，报错回滚")
    void testSaveBatch_split_rollback() {
        // given
        List<UserEo> userEos = generateTestUserEos(RandomUtil.randomInt(10, 21));
        userEos.add(userEos.get(0));
        userEos.addAll(generateTestUserEos(RandomUtil.randomInt(10, 21)));

        // when
        log.info("测试数据长度: {}", userEos.size());
        Assertions.assertThrows(DuplicateKeyException.class, () -> userDas.saveBatch(userEos, 1));

        // then
        Collection<Long> ids = userEos.stream().map(UserEo::getId).toList();
        Assertions.assertEquals(0, userDas.countByIds(ids));
    }

    @Test
    @DisplayName("测试批量保存，忽略错误继续")
    void testSaveBatchIgnoreErrors() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        userEos.add(userEos.get(0));

        // when
        int count = userDas.saveBatchIgnoreErrors(userEos);

        // then
        Assertions.assertEquals(2, count);
    }

    @Test
    @DisplayName("测试批量保存，忽略错误继续 -- 包含null主键")
    void testSaveBatchIgnoreErrors_1() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        UserEo userEoCopy = userEos.get(0).clone();
        userEoCopy.setId(null);
        userEos.add(userEoCopy);

        log.info("测试数据: {}", JSONUtil.toJsonPrettyStr(userEos));
        // when
        var exception = Assertions.assertThrows(IllegalArgumentException.class, () -> userDas.saveBatchIgnoreErrors(userEos));

        // then
        Assertions.assertNotNull(exception);
        log.info(exception.getMessage(), exception);
        Assertions.assertEquals(0, userDas.countByIds(CollUtil.newArrayList(userEos.get(0).getId(), userEos.get(1).getId())));
    }

    @Test
    @DisplayName("测试批量操作BulkOps的Ordered模式 -- 失败后停止")
    void testBulkOps_Ordered() {
        // given
        var user1 = new UserEo();
        user1.setId(IdUtil.getSnowflakeNextId());
        user1.setUserName("user1");
        user1.setEmail("user1@example.com");

        var user2 = new UserEo();
        user2.setId(IdUtil.getSnowflakeNextId());
        user2.setUserName("user2");
        user2.setEmail("user2@example.com");

        // when
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkOperations.BulkMode.ORDERED, UserEo.class);
        bulkOperations.insert(user1).insert(user1).insert(user2);
        // 重复数据，报错，忽略错误继续
        Assertions.assertThrows(BulkOperationException.class, bulkOperations::execute);

        // then
        // 因为ORDERED模式而停止了，只有1条数据插入成功
        Assertions.assertEquals(1, userDas.countByIds(CollUtil.newArrayList(user1.getId(), user2.getId())));
    }

    @Test
    @DisplayName("测试批量操作BulkOps的Ordered模式 -- 失败后继续其他操作")
    void testBulkOps_UnOrdered() {
        // given
        var user1 = new UserEo();
        user1.setId(IdUtil.getSnowflakeNextId());
        user1.setUserName("user1");
        user1.setEmail("user1@example.com");

        var user2 = new UserEo();
        user2.setId(IdUtil.getSnowflakeNextId());
        user2.setUserName("user2");
        user2.setEmail("user2@example.com");

        // when
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, UserEo.class);
        bulkOperations.insert(user1).insert(user1).insert(user2);
        // 重复数据，报错，忽略错误继续
        Assertions.assertThrows(BulkOperationException.class, bulkOperations::execute);

        // then
        // 因为UNORDERED模式，报错后继续，2条数据插入成功
        Assertions.assertEquals(2, userDas.countByIds(CollUtil.newArrayList(user1.getId(), user2.getId())));
    }

    @Test
    @DisplayName("测试批量操作insertAll -- 有异常数据，但成功的会保存文档，没有事务，后续文档失败")
    void testInsertAll() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        userEos.add(userEos.get(0));
        // 添加有效的在最后
        userEos.addAll(generateTestUserEos(1));

        // when
        var exception = Assertions.assertThrows(DuplicateKeyException.class, () -> mongoTemplate.insertAll(userEos));

        // then
        Assertions.assertNotNull(exception);
        log.info(exception.getMessage(), exception);
        // 有异常数据，但成功的会保存文档，没有事务，后续文档失败
        List<Long> ids = CollUtil.newArrayList(userEos.get(0).getId(), userEos.get(1).getId(),
            // 增加异常数据后的数据id作为断言条件
            userEos.get(3).getId());
        Assertions.assertEquals(3, ids.size());
        // 插入了异常数据前的文档
        Assertions.assertEquals(2, userDas.countByIds(ids));
    }

    @Test
    @DisplayName("测试批量保存，忽略错误继续 -- 有异常数据，但成功的会保存文档，没有事务，后续文档失败")
    void testSaveBatchIgnoreErrors_2() {
        // given
        List<UserEo> userEos = generateTestUserEos();
        // 添加重复数据
        userEos.add(userEos.get(0));
        // 添加有效的在最后
        userEos.addAll(generateTestUserEos(1));

        // when
        Assertions.assertEquals(4, userEos.size());
        int count = userDas.saveBatchIgnoreErrors(userEos);

        // then
        Assertions.assertEquals(2, count);
        // 有异常数据，但成功的会保存文档，没有事务，后续文档失败
        List<Long> ids = CollUtil.newArrayList(userEos.get(0).getId(), userEos.get(1).getId(),
            // 增加异常数据后的数据id作为断言条件
            userEos.get(3).getId());
        Assertions.assertEquals(3, ids.size());
        // 插入了异常数据前的文档
        Assertions.assertEquals(2, userDas.countByIds(ids));
    }

    @Test
    @DisplayName("测试空查询条件 -- 包含key")
    void testEmptyCriteria_hasKey() {
        // given
        var criteria = Criteria.where("user");

        Assertions.assertTrue(MongoDbDasHelper.isNotSetValue(criteria));
    }

    @Test
    @DisplayName("测试空查询条件 -- 不包含key")
    void testEmptyCriteria_notKey() {
        // given
        var criteria = new Criteria();

        // when
        Document document = criteria.getCriteriaObject();

        // then
        Assertions.assertTrue(document.isEmpty());
    }

    private List<UserEo> batchSaveTestData(int count) {
        List<UserEo> userEos = generateTestUserEos(count);

        userEos.parallelStream().forEach(userDas::save);
        return userEos;
    }

    private List<UserEo> batchSaveTestData() {
        return batchSaveTestData(2);
    }

    private List<UserEo> generateTestUserEos() {
        return generateTestUserEos(2);
    }

    private List<UserEo> generateTestUserEos(int count) {

        List<UserEo> userEos = CollUtil.newArrayList();

        for (int i = 0; i < count; i++) {
            var user = new UserEo();
            user.setId(IdUtil.getSnowflakeNextId());
            user.setUserName("user" + (i + 1) + "#" + RandomUtil.randomString(6));
            user.setEmail(user.getUserName() + "@example.com");
            userEos.add(user);
        }

        return userEos;
    }
}
