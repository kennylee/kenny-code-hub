package cn.kennylee.codehub.mongodb.das.test.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p> 内存型MongoDB附加配置 </p>
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@TestConfiguration
@ComponentScan(basePackages = "cn.kennylee.codehub.mongodb.das")
public class EmbeddedMongoConfig {

}
