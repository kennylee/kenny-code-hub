package cn.kennylee.codehub.springmvc.objectmapper.jackson.rest.dto;

import cn.kennylee.codehub.springmvc.objectmapper.jackson.JacksonConfig;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * <p> 入参dto </p>
 * <p>Created on 2024/1/27.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Data
@AllArgsConstructor
public class TestJacksonConfigReqDto {
    private Long id;

    private Date creatTime;

    private LocalDateTime modifyTime;

    private LocalDate updateDate;

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(JacksonConfig.DATETIME_FORMAT_PATTERN);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(JacksonConfig.DATETIME_FORMAT_PATTERN);
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(JacksonConfig.DATE_FORMAT_PATTERN);

        return "TestJacksonConfigReqDto{" +
                "id=" + id +
                ", creatTime=" + dateFormat.format(creatTime) +
                ", modifyTime=" + dateTimeFormatter.format(modifyTime) +
                ", updateDate=" + dateFormatter.format(updateDate) +
                '}';
    }
}
