package cn.kennylee.codehub.springmvc.objectmapper.jackson;

import cn.kennylee.codehub.springmvc.objectmapper.jackson.rest.dto.TestJacksonConfigReqDto;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * <p> JacksonConfig单元测试 </p>
 * <p>Created on 2024/1/27.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@AutoConfigureMockMvc
class JacksonConfigTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @org.junit.jupiter.api.Test
    void testObjectMapper() throws Exception {
        val testJacksonConfigReqDto = new TestJacksonConfigReqDto(generateId(), new Date(),
                LocalDateTime.now(), LocalDate.now());
        SimpleDateFormat dateFormat = new SimpleDateFormat(JacksonConfig.DATETIME_FORMAT_PATTERN);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(JacksonConfig.DATETIME_FORMAT_PATTERN);
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(JacksonConfig.DATE_FORMAT_PATTERN);

        Assertions.assertEquals(19, String.valueOf(testJacksonConfigReqDto.getId()).length());

        String json = objectMapper.writeValueAsString(testJacksonConfigReqDto);
        System.out.println(json);

        JsonNode jsonNode = objectMapper.readTree(json);
        String modifyTimeJsonString = jsonNode.get("modifyTime").asText();
        Assertions.assertEquals(dateTimeFormatter.format(testJacksonConfigReqDto.getModifyTime()), modifyTimeJsonString);

        String createTimeString = dateFormat.format(testJacksonConfigReqDto.getCreatTime());
        String modifyTimeString = dateTimeFormatter.format(testJacksonConfigReqDto.getModifyTime());
        String updateDateString = dateFormatter.format(testJacksonConfigReqDto.getUpdateDate());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/test-jackson-config/hello")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(String.valueOf(testJacksonConfigReqDto.getId()))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.creatTime", Matchers.is(createTimeString)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.modifyTime", Matchers.is(modifyTimeString)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.updateDate", Matchers.is(updateDateString)))
        ;
    }

    private static long generateId() {
        int length = 19;

        // 生成一个随机 long 数字，注意限制其位数为 length
        long upperBound = (long) Math.pow(10, length);
        long lowerBound = (long) Math.pow(10, length - 1);

        return ThreadLocalRandom.current().nextLong(lowerBound, upperBound);
    }
}
