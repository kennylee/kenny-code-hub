package cn.kennylee.codehub.springmvc.objectmapper.jackson.rest;

import cn.kennylee.codehub.springmvc.objectmapper.jackson.rest.dto.TestJacksonConfigReqDto;
import cn.kennylee.codehub.springmvc.objectmapper.jackson.rest.dto.TestJacksonConfigRespDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> 测试JacksonConfig </p>
 * <p>Created on 2024/1/27.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@RestController
@RequestMapping("/test-jackson-config")
@Slf4j
public class TestJacksonConfigRest {


    @PostMapping("/hello")
    TestJacksonConfigRespDto hello(@RequestBody TestJacksonConfigReqDto testJacksonConfigReqDto) {
        log.info(testJacksonConfigReqDto.toString());

        Assertions.assertNotNull(testJacksonConfigReqDto.getId());
        Assertions.assertNotNull(testJacksonConfigReqDto.getCreatTime());
        Assertions.assertNotNull(testJacksonConfigReqDto.getModifyTime());
        Assertions.assertNotNull(testJacksonConfigReqDto.getUpdateDate());

        return new TestJacksonConfigRespDto(testJacksonConfigReqDto.getId(), testJacksonConfigReqDto.getCreatTime(),
                testJacksonConfigReqDto.getModifyTime(), testJacksonConfigReqDto.getUpdateDate());
    }
}
