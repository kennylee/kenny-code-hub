package cn.kennylee.codehub.springmvc.objectmapper.jackson.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p> 回调DTO </p>
 * <p>Created on 2024/1/27.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Data
@AllArgsConstructor
public class TestJacksonConfigRespDto {
    private Long id;

    private Date creatTime;

    private LocalDateTime modifyTime;

    private LocalDate updateDate;
}
