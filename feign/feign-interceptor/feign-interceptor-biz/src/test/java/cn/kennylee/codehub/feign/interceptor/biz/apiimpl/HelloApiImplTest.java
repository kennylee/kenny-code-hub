package cn.kennylee.codehub.feign.interceptor.biz.apiimpl;

import cn.kennylee.codehub.feign.interceptor.api.HelloApi;
import cn.kennylee.codehub.feign.interceptor.dto.HelloDto;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * <p> HelloApiImpl单元测试 </p>
 * <p>Created on 2024/1/23.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
class HelloApiImplTest {

    private HelloApi helloApi;
    private ServletRequestAttributes servletRequestAttributes;

    @BeforeEach
    void setUp() {
        helloApi = new HelloApiImpl();
        servletRequestAttributes = Mockito.mock(ServletRequestAttributes.class);

        HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(servletRequestAttributes.getRequest()).thenReturn(httpServletRequest);

        List<String> headers = new ArrayList<>();
        headers.add("Authorization");
        Enumeration<String> headerEnumeration = Collections.enumeration(headers);
        Mockito.when(httpServletRequest.getHeaderNames()).thenReturn(headerEnumeration);

        Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn("test");
    }

    @Test
    void testHello() {
        final String testName = "kenny";

        try (MockedStatic<RequestContextHolder> requestContextHolder = Mockito.mockStatic(RequestContextHolder.class)) {

            requestContextHolder.when(RequestContextHolder::getRequestAttributes)
                .thenReturn(servletRequestAttributes);

            Assertions.assertNotNull(RequestContextHolder.getRequestAttributes());

            HelloDto helloDto = helloApi.hello(testName);

            Assertions.assertNotNull(helloDto);
            Assertions.assertEquals(testName, helloDto.getName());
            Assertions.assertNotNull(helloDto.getHeaders());
            Assertions.assertEquals(1, helloDto.getHeaders().size());
            Assertions.assertEquals("test", helloDto.getHeaders().get("Authorization"));
        }
    }
}
