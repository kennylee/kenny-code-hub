package cn.kennylee.codehub.feign.interceptor.caller.remote.interceptors;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Enumeration;
import java.util.Objects;

/**
 * <p> feign demo interceptor  </p>
 * <p>Created on 2024/1/23.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Slf4j
public class DemoInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        if (Objects.isNull(requestAttributes)) {
            log.warn("RequestAttributes is null");
            return;
        }

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) requestAttributes).getRequest();

        Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = httpServletRequest.getHeader(headerName);
            if (StringUtils.hasLength(headerValue)) {
                log.trace("headerName: {}, headerValue: {}", headerName, headerValue);
                // 传递header
                requestTemplate.header(headerName, headerValue);
            }
        }
    }
}
