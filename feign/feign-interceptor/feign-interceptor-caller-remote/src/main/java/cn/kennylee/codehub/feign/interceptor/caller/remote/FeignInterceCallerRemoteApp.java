package cn.kennylee.codehub.feign.interceptor.caller.remote;

import cn.kennylee.codehub.feign.interceptor.caller.remote.interceptors.DemoInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * <p> 启动入口 </p>
 * <p>Created on 2024/1/22.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootApplication
// FeignClient扫描，实例化客户端
@EnableFeignClients(basePackages = {"cn.kennylee.codehub.feign.interceptor.api"})
public class FeignInterceCallerRemoteApp {
    public static void main(String[] args) {
        SpringApplication.run(FeignInterceCallerRemoteApp.class, args);
    }

    /**
     * 注入拦截器
     *
     * @return DemoInterceptor
     */
    @Bean
    DemoInterceptor demoInterceptor() {
        return new DemoInterceptor();
    }
}
