package cn.kennylee.codehub.feign.interceptor.caller.remote.rest;

import cn.kennylee.codehub.feign.interceptor.api.HelloApi;
import cn.kennylee.codehub.feign.interceptor.dto.HelloDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> 触发调用feign的接口-远程服务调用 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@RestController
@RequestMapping("/caller/remote")
public class HelloAggRemoteController {

    private HelloApi helloApi;

    @GetMapping(value = "/hello/{name}")
    public HelloDto hello(@PathVariable("name") String name) {
        return helloApi.hello(name);
    }

    @Autowired
    public void setHelloApi(HelloApi helloApi) {
        this.helloApi = helloApi;
    }
}
