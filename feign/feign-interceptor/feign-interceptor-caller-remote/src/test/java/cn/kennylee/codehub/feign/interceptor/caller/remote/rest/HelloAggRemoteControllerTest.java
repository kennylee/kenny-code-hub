package cn.kennylee.codehub.feign.interceptor.caller.remote.rest;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * <p> HelloAggRemoteController单元测试 </p>
 * <p>Created on 2024/1/23.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
class HelloAggRemoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        // 需要先手动启动远程服务 feign-interceptor-app
    }

    @Test
    void testHello() throws Exception {
        final String testName = "kenny";
        final String testHeaderValue = "Bearer " + testName;

        mockMvc.perform(MockMvcRequestBuilders.get("/caller/remote/hello/" + testName)
                .header("Authorization", testHeaderValue))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(testName)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.headers.authorization", Matchers.is(testHeaderValue)))
        ;
    }
}
