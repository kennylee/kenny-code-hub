package cn.kennylee.codehub.feign.interceptor.caller.remote;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * <p> FeignInterceCallerRemoteApp单元测试 </p>
 * <p>Created on 2024/1/23.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
class FeignInterceCallerRemoteAppTest {
    @Test
    void contextLoads() {
    }
}
