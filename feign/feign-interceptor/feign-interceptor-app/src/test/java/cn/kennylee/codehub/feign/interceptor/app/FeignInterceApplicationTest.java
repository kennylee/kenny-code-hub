package cn.kennylee.codehub.feign.interceptor.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * <p> FeignInterceApplication单元测试 </p>
 * <p>Created on 2024/1/23.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
class FeignInterceApplicationTest {

    @Test
    void contextLoads() {
    }
}
