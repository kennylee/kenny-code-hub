package cn.kennylee.codehub.feign.interceptor.app.rest;

import cn.kennylee.codehub.feign.interceptor.api.HelloApi;
import cn.kennylee.codehub.feign.interceptor.dto.HelloDto;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> HelloApi controller实现 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@RestController
@RequestMapping("/foo")
@Slf4j
public class HelloApiController implements HelloApi {

    @Resource
    private HelloApi helloApi;

    @Override
    public HelloDto hello(String name) {
        log.info("我在HelloApiController");
        return helloApi.hello(name);
    }
}
