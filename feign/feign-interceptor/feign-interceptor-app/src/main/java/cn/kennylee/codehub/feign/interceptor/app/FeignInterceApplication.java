package cn.kennylee.codehub.feign.interceptor.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p> 启动入口 </p>
 * <p>Created on 2024/1/22.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootApplication(
    scanBasePackages = {
        // 本项目需要扫描的类
        "cn.kennylee.codehub.feign.interceptor.app",
        // controller实现类
        "cn.kennylee.codehub.feign.interceptor.biz.apiimpl",
    }
)
public class FeignInterceApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeignInterceApplication.class, args);
    }
}
