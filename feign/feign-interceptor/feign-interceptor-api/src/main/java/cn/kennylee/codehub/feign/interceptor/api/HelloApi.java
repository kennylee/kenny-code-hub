package cn.kennylee.codehub.feign.interceptor.api;

import cn.kennylee.codehub.feign.interceptor.dto.HelloDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * <p> hello api </p>
 * <p>Created on 2024/1/22.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@FeignClient(
    // 定义指向的领域服务名称
    name = "${code-hub.feign.interceptor.api.name:interceptor-app}",
    // 定义具体bean名字，建议设置，一般同个服务领域下的name是一致的，通过contextId来区分不同的client(api)
    contextId = "cn-kennylee-codehub-feign-interceptor-api-HelloApi",
    // 定义远程调用的url地址前缀，如果为空，会根据服务名来寻找服务，如果有值，优先使用。实际URL地址是，url+方法上的mapping地址; 即该类下的所有接口都会继承该路径
    // 一般为空，通过如nacos等服务发现应用获取具体url指向，只有调试时设置；
    url = "${code-hub.feign.interceptor.url:http://localhost:8081/foo}",
    // 覆盖默认配置，建议为false，表示不是主FeignClient，可以有多个FeignClient，一般设置为false
    primary = false)
public interface HelloApi {

    /**
     * demo
     *
     * @param name 入参
     * @return 返回值
     */
    @GetMapping(value = "/hello/{name}")
    HelloDto hello(@PathVariable("name") String name);
}
