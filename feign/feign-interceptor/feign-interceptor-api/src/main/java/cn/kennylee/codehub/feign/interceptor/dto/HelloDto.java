package cn.kennylee.codehub.feign.interceptor.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * <p> dto </p>
 * <p>Created on 2024/1/22.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Data
@Builder
public class HelloDto {
    private String name;

    private Map<String, String> headers;
}
