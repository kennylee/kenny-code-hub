# Feign使用案例-拦截器

常用在应用层传递header，如token，登录身份等到领域层使用。

## 模块说明

* [feign-interceptor-api](feign-interceptor-api): feign API。
* [feign-interceptor-biz](feign-interceptor-biz): 业务层。
* [feign-interceptor-app](feign-interceptor-app): 远程服务应用。
* [feign-interceptor-caller-remote](feign-interceptor-caller-remote): feign拦截器配置与使用，运行时依赖 `feign-interceptor-app`。

## 说明

* Feign的拦截器配置在 `spring.cloud.openfeign.client.config.requestInterceptors` 可以设置多个，但无法保证其顺序
* 定义的拦截器需要先注入到Spring工厂上下文，否则无法使用。