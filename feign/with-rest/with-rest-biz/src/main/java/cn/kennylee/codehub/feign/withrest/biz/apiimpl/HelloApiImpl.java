package cn.kennylee.codehub.feign.withrest.biz.apiimpl;

import cn.kennylee.codehub.feign.withrest.api.HelloApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * <p> HelloApi feign实现类 </p>
 * <p>Created on 2024/1/15.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Component
@Primary // 建议指定，因为HelloApi的实现类可能有多个，需要指定一个实现类
@Slf4j
public class HelloApiImpl implements HelloApi {

    @Override
    public String hello(String name) {
        log.info("我在HelloApiImpl");
        return "Hello " + name;
    }
}
