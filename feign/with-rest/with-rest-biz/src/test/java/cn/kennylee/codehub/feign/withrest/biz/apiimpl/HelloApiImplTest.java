package cn.kennylee.codehub.feign.withrest.biz.apiimpl;

import cn.kennylee.codehub.feign.withrest.api.HelloApi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * <p> HelloApiImpl单元测试类 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
class HelloApiImplTest {

    private HelloApi helloApi;

    @BeforeEach
    void setUp() {
        helloApi = new HelloApiImpl();
    }

    @Test
    void testHello() {
        final String testName = "kenny";
        Assertions.assertEquals("Hello " + testName, helloApi.hello(testName));
    }
}
