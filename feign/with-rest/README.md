# Feign使用案例-配合SpringMVC

## 模块说明

* [with-rest-api](with-rest-api): 该模块提供了一个REST API，用于提供Feign的测试接口。
* [with-rest-biz](with-rest-biz): 该模块提供了一个REST业务，用于远程调用Feign实现。
* [with-rest-app](with-rest-app): 该模块提供了一个REST应用，用于远程调用Feign实现以及定义controller接口。
* [with-rest-api-caller-local](with-rest-api-caller-local): 该模块提供了一个本地调用Feign的测试接口。
* [with-rest-api-caller-remote](with-rest-api-caller-remote): 该模块提供了一个远程调用Feign的测试接口例子，包含 *日志配置输出*，*连接池* 例子。使用本模块时，需要先启动了 `with-rest-app` 应用。

## 说明

* 关于Feign客户端远程调用时，远程服务对应的入口是spring mvc的controller，而apiImpl是在本地调用的时候使用的；所以一般做法是在controller接口中定义一个方法，用于调用apiImpl中的方法。
* Feign远程调用，一般用到应用层或者的单独部署的聚合层。
* Feign本地调用，一般用在领域层。