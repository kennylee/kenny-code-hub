package cn.kennylee.codehub.feign.withrest.remote;

/**
 * <p> 启动入口 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
// FeignClient扫描，实例化客户端
@EnableFeignClients(basePackages = {"cn.kennylee.codehub.feign.withrest.api"})
public class WithRestCallerRemoteApplication {
    public static void main(String[] args) {
        SpringApplication.run(WithRestCallerRemoteApplication.class, args);
    }
}
