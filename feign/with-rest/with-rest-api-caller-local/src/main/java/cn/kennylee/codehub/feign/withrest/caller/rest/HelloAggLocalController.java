package cn.kennylee.codehub.feign.withrest.caller.rest;

import cn.kennylee.codehub.feign.withrest.api.HelloApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> 触发调用feign的接口-本地调用 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@RestController
@RequestMapping("/caller/local")
public class HelloAggLocalController {

    private HelloApi helloApi;

    @GetMapping(value = "/hello/{name}")
    public String hello(@PathVariable("name") String name) {
        return helloApi.hello(name);
    }

    @Autowired
    public void setHelloApi(HelloApi helloApi) {
        this.helloApi = helloApi;
    }
}
