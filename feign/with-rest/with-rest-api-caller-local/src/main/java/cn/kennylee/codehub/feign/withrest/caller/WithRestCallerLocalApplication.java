package cn.kennylee.codehub.feign.withrest.caller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p> 启动入口 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootApplication(
    scanBasePackages = {
        // 本项目需要扫描的类
        "cn.kennylee.codehub.feign.withrest.caller",
        // 远程服务的接口实现类
        "cn.kennylee.codehub.feign.withrest.biz.apiimpl",
    })
// 使用FeignClient本地调用时实际上可以不需要这个注解，即不用实例化FeignClients，只要当成的本地接口就好了
// 但为了更好的理解FeignClient的primary，还是显示的加上了，实现如果FeignClient有本地实现类时，优先用本地的
@EnableFeignClients(basePackages = {"cn.kennylee.codehub.feign.withrest.api"})
public class WithRestCallerLocalApplication {
    public static void main(String[] args) {
        SpringApplication.run(WithRestCallerLocalApplication.class, args);
    }
}
