package cn.kennylee.codehub.feign.withrest.caller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * <p> WithRestCallerApplication单元测试类 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
class WithRestCallerApplicationTest {
    @Test
    void contextLoads() {
    }
}
