package cn.kennylee.codehub.feign.withrest.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * <p> WithRestApplication单元测试 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
class WithRestApplicationTest {

    @Test
    void contextLoads() {
    }
}
