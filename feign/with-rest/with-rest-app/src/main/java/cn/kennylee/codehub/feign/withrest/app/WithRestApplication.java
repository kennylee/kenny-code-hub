package cn.kennylee.codehub.feign.withrest.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p> 启动入口 </p>
 * <p>Created on 2024/1/18.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootApplication(
    scanBasePackages = {
        // 本项目需要扫描的类
        "cn.kennylee.codehub.feign.withrest.app",
        // controller实现类
        "cn.kennylee.codehub.feign.withrest.biz.apiimpl",
    }
)
public class WithRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(WithRestApplication.class, args);
    }
}
