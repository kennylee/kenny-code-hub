package cn.kennylee.codehub.common.das.dto;

import lombok.Data;

import java.util.List;

/**
 * 分页参数
 * <p>Created on 2024/12/6.</p>
 *
 * @author kennylee
 */
@Data
// @DynamicMaxPage
public class PageDto {

    public static final Integer DEFAULT_PAGE_NO = 1;
    public static final Integer DEFAULT_PAGE_SIZE = 10;

    /**
     * 页码
     */
    private Integer pageNo = DEFAULT_PAGE_NO;

    /**
     * 每页条数
     */
    private Integer pageSize = DEFAULT_PAGE_SIZE;

    /**
     * 是否查询总数 ## null: 查询count+data，true: 仅查询count，false: 仅查询data
     */
    private Boolean searchCount;

    /**
     * 排序条件
     */
    private List<SortDto> sorts;
}
