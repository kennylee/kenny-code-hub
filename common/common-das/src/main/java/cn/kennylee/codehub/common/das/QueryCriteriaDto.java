package cn.kennylee.codehub.common.das;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 每个查询条件
 * <p>Created on 2024/12/11.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Getter
@Setter
@NoArgsConstructor
public class QueryCriteriaDto {
    /**
     * 查询字段名字，驼峰风格
     */
    private String entityPropName;

    /**
     * 逻辑操作
     */
    private CondOperation condOperation;

    /**
     * 查询值
     */
    private Object value;

    @Builder
    public QueryCriteriaDto(String entityPropName, CondOperation condOperation, Object value) {
        this.entityPropName = entityPropName;
        this.condOperation = condOperation;
        this.value = value;
    }

    @Override
    public String toString() {
        return "QueryCriteriaDto{" +
            "entityPropName='" + entityPropName + '\'' +
            ", condOperation=" + condOperation +
            ", value=" + value +
            '}';
    }
}
