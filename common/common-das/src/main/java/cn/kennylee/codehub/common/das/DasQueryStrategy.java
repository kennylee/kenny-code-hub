package cn.kennylee.codehub.common.das;

import java.util.Collection;

/**
 * <p> 查询策略 </p>
 * <p>Created on 2024/12/11.</p>
 *
 * @param <E> 查询条件dto
 * @param <T> entity实体类
 * @author kennylee
 * @since 0.0.1
 */
public interface DasQueryStrategy<E, T> {
    /**
     * 解析条件
     *
     * @param searchExample 查询实例
     * @param entityClass   目标数据库类型或实体类
     * @param ignoreProps   忽略的属性列表
     * @return 生成的查询字符串或对象
     */
    Object parseQuery(E searchExample, Class<T> entityClass, Collection<String> ignoreProps);

    /**
     * 获取实例
     *
     * @return 实例
     */
    DasQueryStrategy<E, T> newInstance();
}
