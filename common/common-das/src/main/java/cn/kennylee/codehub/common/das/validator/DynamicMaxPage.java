package cn.kennylee.codehub.common.das.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p> 动态限制最大分页的validation注解 </p>
 * <p>Created on 2025/2/13.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MaxPageValidator.class)
public @interface DynamicMaxPage {
    String message() default "页码超出最大允许值";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 页码字段名
     *
     * @return 页码字段名
     */
    String pageField() default "pageNo";

    /**
     * 分页大小的字段名
     *
     * @return 分页大小的字段名
     */
    String sizeField() default "pageSize";

    /**
     * 限制的最大可分页查询的记录数，默认为10w
     *
     * @return 限制的总查询记录数
     */
    int totalRecords() default 100000;
}
