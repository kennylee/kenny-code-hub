package cn.kennylee.codehub.common.das.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义业务分页对象
 * <p>Created on 2025/2/5.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Getter
@Setter
public class CustomPageDto<T> {
    /**
     * 每页显示多少条记录
     */
    private long pageSize;

    /**
     * 当前第几页数据
     */
    private long pageNo;

    /**
     * 一共多少条记录
     */
    private long total;

    /**
     * 一共多少页
     */
    private long totalPage;

    /**
     * 要显示的数据
     */
    private List<T> data = new ArrayList<>();

    private CustomPageDto() {
    }

    public CustomPageDto(long pageNo, long pageSize, long total, long totalPage, List<T> list) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.total = total;
        this.totalPage = totalPage;
        this.data = list;
    }

    public CustomPageDto(long pageNo, long pageSize, List<T> list) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        if (list == null || list.isEmpty()) {
            return;
        }
        this.data = list;
    }

    /**
     * 分页对象封装
     */
    public CustomPageDto(long pageNo, long pageSize, long total, List<T> list) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        if (list == null || list.isEmpty()) {
            return;
        }
        this.total = total;
        this.data = list;
        // 获取总页数
        long pages = this.total / this.pageSize;
        if (this.total % this.pageSize != 0) {
            // 如果总页数有余数，则总页数加1
            pages = pages + 1;
        }
        this.totalPage = pages;
    }

    public static <T> CustomPageDto<T> emptyPage() {
        return new CustomPageDto<>();
    }
}
