package cn.kennylee.codehub.common.das.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> 分页排序参数 </p>
 * <p>Created on 2024/12/6.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class SortDto {
    public static final String DESC = "DESC";
    public static final String ASC = "ASC";

    /**
     * 排序字段，对应数据库字段，一般是下划线风格命名
     */
    private String field;

    /**
     * 排序规则 ## 升序或降序
     */
    private String direction;
}
