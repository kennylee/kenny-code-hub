package cn.kennylee.codehub.common.das;

/**
 * <p> das策略类型枚举 </p>
 * <p>Created on 2024/12/11.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
public enum DasDbType {
    /**
     * mysql
     */
    MYSQL,
    /**
     * mongodb
     */
    MONGODB;
}
