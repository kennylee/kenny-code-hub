package cn.kennylee.codehub.drools.compilerspring.utils;

import org.drools.core.spi.KnowledgeHelper;
import org.slf4j.LoggerFactory;

/**
 * <p> 规则引擎工具类 </p>
 * <p>Created on 2024/3/17.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
public class RuleFunctions {

    public static void log(final KnowledgeHelper drools, final String message,
                           final Object... parameters) {
        final String category = drools.getRule().getPackageName() + "."
            + drools.getRule().getName();
        final String formattedMessage = String.format(message, parameters);
        LoggerFactory.getLogger(category).info(formattedMessage);
    }
}
