package cn.kennylee.codehub.drools.compilerspring.dto;

import lombok.Data;

/**
 * <p> 订单实例 </p>
 * <p>Created on 2024/3/16.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Data
public class OrderDto {

    /**
     * 订单金额
     */
    private int price;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单用户
     */
    private UserDto userDto;

    /**
     * 优惠金额
     */
    private Integer discount;
}
