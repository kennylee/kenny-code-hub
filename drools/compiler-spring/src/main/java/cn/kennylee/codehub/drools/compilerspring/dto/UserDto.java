package cn.kennylee.codehub.drools.compilerspring.dto;

import lombok.Data;

/**
 * <p> 用户信息 </p>
 * <p>Created on 2024/3/16.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@Data
public class UserDto {
    private Long id;

    /**
     * 用户名
     */
    private String userName;
}
