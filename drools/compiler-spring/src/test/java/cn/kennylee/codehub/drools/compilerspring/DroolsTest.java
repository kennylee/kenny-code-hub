package cn.kennylee.codehub.drools.compilerspring;

import cn.hutool.core.util.RandomUtil;
import cn.kennylee.codehub.drools.compilerspring.dto.OrderDto;
import jakarta.annotation.Resource;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * <p> Drools单元测试 </p>
 * <p>Created on 2024/3/16.</p>
 *
 * @author kennylee
 * @since 0.0.1
 */
@SpringBootTest
public class DroolsTest {

    @Resource
    private KieBase kieBase;

    /**
     * 查询全部规则，订单金额小于100，匹配成功
     */
    @Test
    public void testMatchLess100() {
        KieSession kieSession = kieBase.newKieSession();

        OrderDto orderDto = new OrderDto();
        orderDto.setPrice(RandomUtil.randomInt(100));

        kieSession.insert(orderDto);
        Assertions.assertNull(orderDto.getDiscount());

        kieSession.fireAllRules();
        kieSession.dispose();

        Assertions.assertEquals(1, orderDto.getDiscount());
    }

    /**
     * 根据规则名前缀查询，订单金额小于100，匹配成功
     */
    @Test
    public void testMatchLess100ByRuleName() {
        KieSession kieSession = kieBase.newKieSession();

        OrderDto orderDto = new OrderDto();
        orderDto.setPrice(RandomUtil.randomInt(100));

        kieSession.insert(orderDto);
        Assertions.assertNull(orderDto.getDiscount());

        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("order_discount"));
        kieSession.dispose();

        Assertions.assertEquals(1, orderDto.getDiscount());
    }

    /**
     * 根据规则名前缀查询，订单金额小于100，匹配失败；规则名不匹配
     */
    @Test
    public void testNotMatchLess100ByRuleName() {
        KieSession kieSession = kieBase.newKieSession();

        OrderDto orderDto = new OrderDto();
        orderDto.setPrice(RandomUtil.randomInt(100));

        kieSession.insert(orderDto);
        Assertions.assertNull(orderDto.getDiscount());

        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter(RandomUtil.randomString(5)));
        kieSession.dispose();

        Assertions.assertNull(orderDto.getDiscount());
    }

    /**
     * 查询全部规则，订单金额小于100，匹配成功
     */
    @Test
    public void testMatchLess200() {
        KieSession kieSession = kieBase.newKieSession();

        OrderDto orderDto = new OrderDto();
        orderDto.setPrice(RandomUtil.randomInt(100, 200));

        kieSession.insert(orderDto);

        Assertions.assertNull(orderDto.getDiscount());

        kieSession.fireAllRules();
        kieSession.dispose();

        Assertions.assertEquals(2, orderDto.getDiscount());
    }

    /**
     * 查询全部规则，找不到匹配的规则
     */
    @Test
    public void testAllNotFound() {
        KieSession kieSession = kieBase.newKieSession();

        OrderDto orderDto = new OrderDto();
        orderDto.setPrice(RandomUtil.randomInt(10000, 99999));

        kieSession.insert(orderDto);

        Assertions.assertNull(orderDto.getDiscount());

        kieSession.fireAllRules();
        kieSession.dispose();

        Assertions.assertNull(orderDto.getDiscount());
    }

    /**
     * 根据包名前缀查询，订单金额小于100，匹配成功
     */
    @Test
    public void testMatchLess100ByPackageName() {
        KieSession kieSession = kieBase.newKieSession();

        OrderDto orderDto = new OrderDto();
        orderDto.setPrice(RandomUtil.randomInt(100));

        kieSession.insert(orderDto);
        Assertions.assertNull(orderDto.getDiscount());

        kieSession.fireAllRules((match -> match.getRule().getPackageName().equals("cn.kennylee.codehub.drools.compilerspring.rules.order")));
        kieSession.dispose();

        Assertions.assertEquals(1, orderDto.getDiscount());
    }

    /**
     * 根据包名前缀查询，订单金额小于100，匹配失败；没找到包名的规则
     */
    @Test
    public void testNotMatchLess100ByPackageName() {
        KieSession kieSession = kieBase.newKieSession();

        OrderDto orderDto = new OrderDto();
        orderDto.setPrice(RandomUtil.randomInt(100));

        kieSession.insert(orderDto);
        Assertions.assertNull(orderDto.getDiscount());

        kieSession.fireAllRules((match -> match.getRule().getPackageName().equals(RandomUtil.randomString(5))));
        kieSession.dispose();

        Assertions.assertNull(orderDto.getDiscount());
    }
}
