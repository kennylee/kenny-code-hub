# Kenny小狼的代码盒子

存放日常使用的代码片段，方便以后查阅。以单个模块为主，重复代码优先考虑复制粘贴，减少不同例子之间的耦合度。

> 大龄码农，虽然有些公司不友善，不过也通过这些小小代码回馈下社会，希望对大家能有帮助。

see also: https://gitee.com/kennylee/kenny-script-hub

## 目录

### [cache](cache)

  * [cache-service](cache/cache-service) 缓存服务，提供缓存服务的封装，提供缓存服务的统一接口。包括多级缓存和解决缓存穿透等问题。

### [Feign](feign) 

  * [feign-interceptor](feign%2Ffeign-interceptor) 拦截器，常用在应用层传递header，如token，登录身份等到领域层使用。
  * [with-rest](feign%2Fwith-rest) 结合SpringMVC使用，远程与本地调用案例。

### [MyBatis-Plus](mybatis) 

  * [base-usage](mybatis%2Fbase-usage) 最小化配置例子，单元测试用h2
  * [code-generator](mybatis/code-generator) 代码生成器，生成entity、mapper、service、controller、自定义代码等
  * [mybatis-das](mybatis/mybatis-das) MySQL数据访问层，提供基本的增删改查操作

### [springboot](springboot)

  * [lifecycle-logger](springboot/lifecycle-logger) SpringBoot应用生命周期日志增强器

### [SpringMVC](springmvc) 
 
  * [object-mapper](springmvc%2Fobject-mapper) JSON对象映射配置

### [mongodb](mongodb)

  * [mongodb-das](mongodb/mongodb-das) MongoDB数据访问层，提供MongoDB的基本操作，包括增删改查等。

### [drools](drools)

  * [compiler-spring](drools/compiler-spring) SpringBoot环境下使用drools规则引擎

### [tools](tools)

  * [dependency-check](tools/dependency-check) Maven添加dependency-check的task，实现依赖检查，检查项目中的依赖是否有漏洞